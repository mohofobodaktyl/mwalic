package walic.pureEngine



import kotlin.random.Random

abstract class PlayerAdapter {



    abstract fun setRequierd(state: State, noDeck: Boolean = false): Pair<List<String>, Long>
    abstract suspend fun printMess(printOption: State.PrintOption, card: Card?)

    abstract suspend fun pickRow(mask: RowMask): CardLocation
    abstract suspend fun pickPlaceBetweenCards(card: Card, mask: RowMask): CardLocation
    abstract suspend fun <A> pickFromList(list: List<A>, n: Int = 1, canLess: Boolean = false): List<Int>


    open fun nextRandom(): Long {
        return Random.nextLong()
    }



}
//
//fun readFile(file: File): List<String> {
//    return file.readLines().drop(1).filter(String::isNotBlank).map(String::trim)
//}






