package walic.pureEngine

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.random.Random


class SteamAdapter(val inputStream: suspend () -> String, val outputStream: suspend (String) -> Unit, val playerNumber: Int, var playerAdapter: PlayerAdapter, private val debugLevel: Int) {
    val secretInformationHandler = SecretInformationHandler(playerNumber)

    var printWaiting = suspend {}

    val otherInputs = mutableListOf<String>()
    suspend fun readVal(): String {
        val ret = inputStream()
        otherInputs.add(ret)
        return ret
    }

    val myOthputs = mutableListOf<String>()
    suspend fun writeVal(string: String) {
        myOthputs.add(string)
        outputStream(string)
    }

    private val radnomLog = mutableListOf<Long>()
    fun getNextRandomSeed(): Long {
        val localRandom = playerAdapter.nextRandom()
        radnomLog.add(localRandom)
        return localRandom
    }

    suspend fun verify() {
        outputStream(Json.encodeToString(myPaOutputs))
        val otherPaInputs: MutableList<String> = Json.decodeFromString(inputStream())
        outputStream(Json.encodeToString(radnomLog))
        val otherRandomLong: MutableList<Long> = Json.decodeFromString(inputStream())
        outputStream(Json.encodeToString(exEntropy))
        val otherExEntropy: MutableList<Long> = Json.decodeFromString(inputStream())

        val otherReplayAdapter = Replay(otherPaInputs, myOthputs, otherRandomLong, 1 - playerNumber).getStreamAdapter(otherAdapter = null, quiet = true, debugLevel = 0)
        val state = State(otherReplayAdapter, false)
        state.mainLoop()

        (otherReplayAdapter.myPaOutputs zip otherPaInputs).forEach { (p1, p2) ->
            if (p1 != p2)
                throw WrongInputException("Błąd werifikacjie '$p1' != '$p2'")
        }
        (paHashes zip (otherPaInputs zip otherExEntropy).map { it.secureHash() }).forEach { (p1, p2) ->
            if (p1 != p2)
                throw WrongInputException("Błąd werifikacjie haszy '$p1' != '$p2'")

        }
        (otherReplayAdapter.allOutputs zip allOutputs).forEach { (p1, p2) ->
            if (p1 != p2)
                throw WrongInputException("Błąd werifikacjie allOutputs'$p1' != '$p2'")
        }
    }

    var random: Random = Random(0)
    suspend fun setNewRandom() {
        val localRandom = getNextRandomSeed()
        writeVal(localRandom.toString())
        val recivedRandom = readVal().toLong()
        random = Random(recivedRandom + localRandom)

    }


    val newRandomRequierdConstat = "the industrial revolution has been a disaster for the human race"


    fun checkIfMakesSens(state: State) {
        if (debugLevel > 0) {
            val stateHand = state.players[playerNumber].knownHand
            val sihHand = secretInformationHandler.cardInHand
            val sihDeck = secretInformationHandler.cardToDraw
            if (sihDeck.distinct().size != sihDeck.size)
                throw WrongInputException("Podwójna w deku $sihDeck")
            if (sihHand.distinct().size != sihHand.size)
                throw WrongInputException("Podwójna w ręce $sihHand")
            if (sihHand.intersect(sihDeck).isNotEmpty())
                throw WrongInputException("Kopia w deck i ręce ${sihHand.intersect(sihDeck)}")
            if (state.players[0].knownHand.intersect(state.players[1].knownHand).isNotEmpty())
                throw WrongInputException("Kopia w obydwu rękach ${sihHand.intersect(sihDeck)}")
            if (sihHand.size != stateHand.size)
                throw WrongInputException("Desnych lol ręka $stateHand!=$sihHand")
            if (debugLevel > 1)
                (stateHand zip sihHand).forEach { (stateCard, sihCard) ->
                    if (stateCard.ac !is UnknowCard) {
                        val stateJSon = Json.encodeToString(stateCard)
                        val sihJson = Json.encodeToString(sihCard)
                        if (stateJSon != sihJson)
                            throw WrongInputException("Desnych lol ręka state-'$stateJSon'!=sih-'$sihJson'")
                    }
                }
            if (sihHand.any { it.ac is UnknowCard })
                throw WrongInputException("Lol Unknow w ręce")
            if (secretInformationHandler.cardToDraw.any { it.ac is UnknowCard })
                throw WrongInputException("Lol Unknow w do dobrania")
            if (state.getAlCardsOnBattleFiled().any { it.ac is UnknowCard || it.state == null })
                throw WrongInputException("Lol Unknow na polu lub null")
            if (state.players.any { p -> p.graveyard.any { it.ac is UnknowCard || it.state == null } })
                throw WrongInputException("Lol Unknow na cementarzu lub null")
        }
    }

    private val myPaOutputs = mutableListOf<String>()
    private val paHashes = mutableListOf<String>()
    private val exEntropy = mutableListOf<Long>()
    private val allOutputs = mutableListOf<String>()
    suspend fun fancyGIWUI(playerID: Int, function: suspend (suspend (suspend (PlayerAdapter) -> String) -> String, SecretInformationHandler) -> String): String {
        var ret: String
        when (playerID) {
            playerNumber -> {
                val f1: suspend (suspend (PlayerAdapter) -> String) -> String = {
                    writeVal(newRandomRequierdConstat)
                    val paOutputString = it.invoke(playerAdapter)
                    val newRandomSeed = readVal().toLong()
                    val enropy = Random.nextLong()

                    exEntropy.add(enropy)
                    writeVal(Pair(paOutputString, enropy).secureHash())
                    myPaOutputs.add(paOutputString)

                    secretInformationHandler.setRandom(newRandomSeed)
                    paOutputString
                }
                ret = function(f1, secretInformationHandler)
                writeVal(ret)
            }
            1 - playerNumber -> {
                while (true) {
                    printWaiting()
                    ret = readVal()
                    if (ret != newRandomRequierdConstat)
                        break
                    val localRAndom = getNextRandomSeed()
                    writeVal(localRAndom.toString())
                    paHashes.add(readVal())
                }
            }
            else -> throw WrongInputException("playerId =$playerID")
        }
        setNewRandom()
        allOutputs.add(ret)
        return ret
    }

     suspend fun fancyGetInput(playerID: Int, function: suspend (PlayerAdapter) -> String): String = fancyGIWUI(playerID) { eval, _ -> eval { function(it) } }
     suspend fun getInfoFancy(playerID: Int, function: (SecretInformationHandler) -> String): String = fancyGIWUI(playerID) { _, sih -> function(sih) }
     suspend fun applyAction(playerID: Int, function: (SecretInformationHandler) -> Unit) = fancyGIWUI(playerID) { _, sih -> function(sih);"" }
    fun getReplay(): String = Json.encodeToString(Replay(myPaOutputs.toMutableList(), otherInputs, radnomLog, playerNumber))

}

private fun Pair<String, Long>.secureHash(): String {
//    return "Hash of '${toString()}'"
    return toString().sumBy { it.toInt() }.toString()
}

fun creatReplyFromString(string: String): Replay {
    return Json.decodeFromString(string)
}

@Serializable
data class Replay(val paInputs: List<String>, val recivedOverNetwork: List<String>, val radnomLog: List<Long>, val playerNumber: Int) {
    fun getStreamAdapter(otherAdapter: PlayerAdapter? = null, quiet: Boolean = false, debugLevel: Int = 2): SteamAdapter {
        var otherInputsCounter = 0
        return SteamAdapter(
            { recivedOverNetwork[otherInputsCounter++] },
            {},
            playerNumber,
            ReplayAdapter(this, otherAdapter, quiet),
            debugLevel
        )
    }

    class ReplayAdapter(private val replay: Replay, private val otherAdapter: PlayerAdapter?, val quiet: Boolean = false) : PlayerAdapter() {
        private var counter = 0
        private var radnomCounter = 0
        override fun setRequierd(state: State, noDeck: Boolean): Pair<List<String>, Long> {
            otherAdapter?.setRequierd(state, noDeck = true)
            return Json.decodeFromString(replay.paInputs[counter++])
        }

        override suspend fun printMess(printOption: State.PrintOption, card: Card?) {
            otherAdapter?.printMess(printOption, card)
            if (!quiet)
                println("$printOption by $card")
        }

        override suspend fun pickRow(mask: RowMask): CardLocation {
            return Json.decodeFromString(replay.paInputs[counter++])
        }

        override suspend fun pickPlaceBetweenCards(card: Card, mask: RowMask): CardLocation {
            return Json.decodeFromString(replay.paInputs[counter++])
        }

        override suspend fun <A> pickFromList(list: List<A>, n: Int, canLess: Boolean): List<Int> {
            return Json.decodeFromString(replay.paInputs[counter++])
        }

        override fun nextRandom(): Long {
            return replay.radnomLog[radnomCounter++]
        }
    }
}


 suspend inline fun <reified A, reified B> SteamAdapter.getInfoWithUserInput(playerID: Int, crossinline function: suspend (suspend (suspend (PlayerAdapter) -> A) -> A, SecretInformationHandler) -> B): B =
    Json.decodeFromString<B>(fancyGIWUI(playerID) { eval, secretInformationHandler -> Json.encodeToString<B>(function({ Json.decodeFromString<A>(eval { playerAdapter -> Json.encodeToString<A>(it(playerAdapter)) }) }, secretInformationHandler)) })

 suspend inline fun <reified B> SteamAdapter.getInput(playerID: Int, crossinline function: suspend (PlayerAdapter) -> B): B =
    Json.decodeFromString(fancyGetInput(playerID) { Json.encodeToString(function(it)) })

 suspend inline fun <reified A> SteamAdapter.getInfo(playerID: Int, crossinline function: (SecretInformationHandler) -> A): A =
    Json.decodeFromString<A>(getInfoFancy(playerID) { Json.encodeToString<A>(function(it)) })


