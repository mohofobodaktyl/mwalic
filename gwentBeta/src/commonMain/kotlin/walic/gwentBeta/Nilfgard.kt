package walic.gwentBeta

import walic.pureEngine.*

val nilfLeaders = listOf(
    EmhyrvarEmreis,
    JanCalveit,
    MorvranVoorhis,
)

val nilfCards = listOf(
    CahirDyffryn,
    LethoKingslayer,
    LeoBonhart,
    MennoCoehoorn,
    RainfarnofAttre,
    Shilard,
    StefanSkellen,
    TiborEggebracht,
    VattierdeRideaux,
    Vilgefortz,
    Albrich,
    AssirevarAnahid,
    Auckes,
    Cadaverine,
    Cantarella,
    Cynthia,
    DazhbogRunestone,
    FringillaVigo,
    HeftyHelge,
    NilfgaardianGate,
    PeterSaarGwynleve,
    Treason,
    Vreemde,
    Vrygheff,
    AlbaArmoredCavalry,
    AlbaPikeman,
    Alchemist,
    Ambassador,
    Assasin,
    CombatEngineer,
    DaerlanSoldier,
    DeithwenArbalest,
    Emissary,
    ExperimentalRemedy,
    FireScorpion,
    ImperaBrigade,
    MagneDivision,
    NauzicaaBrigade,
    Ointment,
    YenneferDivination,
    SlaveDriver,
    SlaveHunter,
    Spotter,
    VenendalElite,
    VicovaroNovice,
    LethoofGulet,
    JoachimdeWett,
    CeallachDyffryn,
    Serrit,
    Sweers,
    Infiltrator,
    NauzicaaSergeant,
    NilfgaardianKnight,
    Recruit,
    SlaveInfantry,
    StandardBearer,
    Mangonel,
    ImperaEnforcers,
    HenryvarAttre,
    Vanhemar,
    RotTosser,
    MasterofDisguise,
    Sentry,
    YenneferEnchantress,
    TheGuardian,
    Xarthisius,
    FalseCiri,
    ViperWitcher,
    ImperialGolem
)


private object EmhyrvarEmreis : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Leader, Tag.Officer)
    override val description = "Play a card from your hand, then return a Bronze or Silver ally to your hand"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.forceToPlay(playerId)
        val pickedCard = state.pickCardOnBattleFiled(playerId, mAlly) {
            it.ist(Tag.Bronze) || it.ist(Tag.Silver)
        } ?: return
        pickedCard.addToHand(playerId)
    }
}


private object JanCalveit : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Leader, Tag.Officer)
    override val description = "Look at 3 cards from your deck, then play 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.drawNPick1(playerId, 3, false, DrawNPickMMods.ToGame) ?: return
        card.playCardwithBattlecryAndEverything(playerId, PlayMod.FromDeck)
    }
}


private object MorvranVoorhis : AbstractCard() {

    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Leader, Tag.Officer)
    override val description = "Reveal up to 4 cards"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(4)
        {
            state.revalCard(playerId, TargetMods.Any)
        }
    }
}


private object Usurper : AbstractCard() {
    //todo . Create any Leader and boost
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Leader, Tag.Officer, Tag.SPY)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*1 Strength, Leader, Officer, Legendary.
*Spying. Create any Leader and boost it by 2.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

    }
}


private object CahirDyffryn : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Doomed, Tag.Gold)
    override val description = "Resurrect a Leader"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId) { it.ist(Tag.Leader) } ?: return
        card.resurrect(playerId)
    }
}


private object LethoofGulet : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Witcher, Tag.Gold, Tag.SPY)
    override val description = "Toggle 2 units' Lock on the row, then Drain all their power."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val mask = maskOnlyRow(playerId, thisCard.location())
        val card1 = state.pickCardOnBattleFiled(playerId, mask) { it != thisCard } ?: return
        val card2 = state.pickCardOnBattleFiled(playerId, mask) { it != card1 && it != thisCard }
        card1.toggleLock()
        card1.toggleLock()
        state.drain(card1, thisCard)
        if (card2 != null)
            state.drain(card2, thisCard)
    }
}


private object LethoKingslayer : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Witcher, Tag.Gold)
    override val description = """Choose One: 
Destroy an enemy Leader, then boost self by 5;
Play a Bronze or Silver Tactic from your deck."""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, description.split("\n").drop(1))[0]) {
            0 -> {
                state.pickCardOnBattleFiled(playerId, mEnemy) { it.ist(Tag.Leader) }
                thisCard.boost(5)
            }
            1 -> {
                state.summon(playerId) { it.ist(Tag.Tactic) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
            }
        }
    }
}


private object LeoBonhart : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Gold)
    override val description = "Reveal one of your units and deal damage equal to its base power to an enemy"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.revalCard(playerId, TargetMods.Ally) ?: return
        state.pickCardOnBattleFiled(playerId)?.dealDamage(card.baseStrength)
    }
}


private object MennoCoehoorn : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Gold)
    override val description = "Damage an Enemy by 4.If spy destory it"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId) ?: return
        card.dealDamage(5)
        if (card.ist(Tag.SPY))
            card.destroy()
    }
}


private object RainfarnofAttre : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Gold)
    override val description = "Play a Bronze or Silver Spying unit from your deck."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { it.ist(Tag.SPY) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
    }
}


private object Shilard : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Gold)
    override val description = "Truce: Draw a card from both decks. Keep one and give the other to your opponent"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        //Trochę informacji
        if (state.isTruce()) {
//            val cardList = listOf(
//                state.drawNPick1(playerId, 1, top = true, mod = DrawNPickMMods.ToGame) ?: return,
//                state.drawNPick1(1 - playerId, 1, top = true, mod = DrawNPickMMods.ToGame) ?: return
//            )
//            val cardYou = state.pickOneOfCard(playerId, cardList)
//            cardList.first { it!=cardYou }.addToHand(playerId)
            val card1 = state.drawNPick1(playerId, 1, top = true, mod = DrawNPickMMods.ToGame) ?: return
            val card2 = state.drawNPick1(1 - playerId, 1, top = true, mod = DrawNPickMMods.ToGame) ?: return
            if (state.pickIf(playerId, "Do you want $card1. If not you will get $card2")) {
                card1.addToHand(playerId)
                card2.addToHand(1 - playerId)
            } else {
                card2.addToHand(playerId)
                card1.addToHand(1 - playerId)
            }


        }
    }
}


private object StefanSkellen : AbstractCard() {

    override var defaultStrenght = 10
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Gold)
    override val description = "Choose a card from your deck, boost it by 5 and move it to the top of your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.applyActionAndMoveToTop(playerId) { it.currentStrength += 5 }

    }
}


private object TiborEggebracht : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Gold)
    override val description = "Truce: Boost self by 15, then your opponent draws a Revealed Bronze card."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.isTruce()) {
            thisCard.boost(15)
            state.draw(1 - playerId, 1, filter = { it.ist(Tag.Bronze) }, realved = true)
        }

    }
}


private object VattierdeRideaux : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Gold)
    override val description = "Reveal up to 2 of your cards, then Reveal the same number of your opponent's randomly."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val howMany = state.pickNofM(playerId, 1, listOf("0", "1", "2"))[0]
        repeat(howMany) {
            state.revalCard(playerId, TargetMods.Ally)
        }
        repeat(howMany) {
            state.revalCard(playerId, TargetMods.Enemy) { list, random -> list.indices.random(random) }
        }

    }
}


private object Vilgefortz : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Gold)
    override val description = """Choose One 
                                 Destroy an ally, then play a card from your deck;
                                  Truce Destroy an enemy, then your opponent draws a Revealed Bronze card.""".trimMargin()

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.isTruce() && state.pickIf(playerId, "Do you want to destory an enemy")) {
            state.pickCardOnBattleFiled(playerId)?.destroy() ?: return
            state.draw(1 - playerId, 1, { it.ist(Tag.Bronze) }, realved = true)
        } else {
            state.pickCardOnBattleFiled(playerId, mAlly)?.destroy() ?: return
            state.drawNPick1(playerId,1,true, DrawNPickMMods.ToGame)?.playCardwithBattlecryAndEverything(playerId,PlayMod.FromDeck)
        }
    }
}


private object Xarthisius : AbstractCard() {
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Gold)
    override val description = "Look at your opponent's deck and move a card to the bottom"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val cards = state.steamAdapter.getInfo(1 - playerId) { it.cardToDraw }.toMutableList()
        val index = cards.indexOf(state.pickOneOfCard(playerId, cards))
        state.steamAdapter.applyAction(1 - playerId) { it.moveToBottom(it.cardToDraw[index]) }
    }
}


private object YenneferEnchantress : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Aedirn, Tag.Gold)
    override val description = "Spawn the last Bronze or Silver Spell you played."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.cardPlayedHistory.filter { it.second == playerId }.map { it.first }.filter { it.ist(Tag.Bronze) || it.ist(Tag.Silver) }.lastOrNull { it.ist(Tag.Spell) }?.spawn(state, playerId)
    }
}


private object Albrich : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Silver)
    override val description = "Truce: Each player draws a card. The opponent's card is Revealed."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.isTruce()) {
            state.draw(playerId, 1)
            state.draw(1 - playerId, 1, realved = true)
        }
    }
}


private object AssirevarAnahid : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Silver)
    override val description = "Return 2 Bronze or Silver cards from either graveyard to their respective decks."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        for (b in listOf(true, true, false, false)) {
            val card = state.pickFromGraviard(playerId, if (b) TargetMods.Enemy else TargetMods.Ally) { (it.ist(Tag.Silver) && it.ist(Tag.Bronze)) && !it.ist(Tag.SPECIAL) }
                ?: continue
            card.addToDeck(if (b) 1 - playerId else playerId)
        }
    }
}


private object Auckes : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Witcher, Tag.Silver)
    override val description = "Lock 2 Units"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            state.pickCardOnBattleFiled(playerId, mAny)?.toggleLock()
        }
    }
}


private object Cadaverine : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Nilfgaard, Tag.SPECIAL, Tag.Item, Tag.Silver, Tag.Alchemy)
    override val description = """Choose one: 
Deal 2 damage to an enemy and all units that share its categories;
 or destroy a Bronze or Silver Neutral unit.
 """

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, description.split("\n").drop(1))[0]) {
            0 -> {
                val card = state.pickCardOnBattleFiled(playerId) ?: return
                val tags2 = card.tags.filter { it != Tag.Bronze && it != Tag.Silver }.toSet()
                state.getAlCardsOnBattleFiled(1 - playerId).forEach {
                    if (tags2.intersect(it.tags).isNotEmpty()) {
                        it.dealDamage(2)
                    }
                }
            }
            1 -> {
                state.pickCardOnBattleFiled(playerId) {
                    it.fraction == null && it.ist(Tag.Silver) && it.ist(Tag.Bronze)
                }?.destroy()
            }
        }
    }
}


private object Cantarella : AbstractCard() {
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Nilfgaard, Tag.Agent, Tag.Silver, Tag.SPY, Tag.SINGLEUSE)
    override val description = "Draw 2 cards. Keep one and move the other to the bottom of your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPickM(playerId, 2, 1, true, DrawNPickMMods.ToHandRestBootom)
    }
}


private object CeallachDyffryn : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Silver)
    override val description = "Spawn an Ambassador, Assassin or Emissary."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(Ambassador, Emissary, Assasin))

    }
}


private object Cynthia : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Silver)
    override val description = " Reveal the Highest Unit in your opponent's Hand and Boost self by its Power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.revalCard(playerId, TargetMods.Enemy) { list, _ ->
            val card = list.filter { !it.ist(Tag.SPECIAL) }.maxByOrNull { it.currentStrength }
            return@revalCard list.indexOf(card)
        } ?: return
        thisCard.boost(card.currentStrength)
    }
}


private object DazhbogRunestone : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Nilfgaard, Tag.SPECIAL, Tag.Item, Tag.Silver, Tag.Alchemy)
    override val description = "Create a Bronze or Silver Nilfgaard card"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.Nilfgaard) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
    }
}


private object FalseCiri : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Silver, Tag.SPY)
    override val description = "Spying. If Spying, boost self by 1 on turn start and when your opponent passes, move to the opposite row. Deathwish: Destroy the Lowest unit on the row.}}"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.counter = 1
    }

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        if (thisCard.counter == 1) {
            if (state.players[playerId].didPass) {
                thisCard.charm()
                thisCard.counter = 0
            } else
                thisCard.boost(1)
        }
    }

    override suspend fun onDeath(state: State, thisCard: Card) {
        state.getRowByLocation(thisCard.location()).filter { it != thisCard }.allMin { it.baseStrength }.randomOrNull(state.getRng())?.destroy()

    }

}


private object FringillaVigo : AbstractCard() {

    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Silver)
    override val description = "Copy the power from the unit to the left to the unit to the right. Spying"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val localtion = thisCard.location()
        val left = state.getCardByLocation(localtion.left()) ?: return
        val right = state.getCardByLocation(localtion.right()) ?: return
        right.setCurrentStrenght(left.currentStrength)

    }
}


private object HeftyHelge : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Machine, Tag.Silver)
    override val description = "Deploy: If this Unit was Revealed, Damage all Enemies by 1. Otherwise, Damage all Enemies except those on opposite row by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = thisCard.location().rowId
        state.players[1 - playerId].rows.filterIndexed { index, _ -> index != row || auxData.playMod == PlayMod.FromHandReveled }.forEach { list -> list.toMutableList().forEach { it.dealDamage(1) } }
    }
}


private object HenryvarAttre : AbstractCard() {

    override var defaultStrenght = 9
    override val tags = listOf(Tag.Nilfgaard, Tag.Support, Tag.Silver)
    override val description = "Conceal any number of units. If allies, boost by 2. If enemies, deal 2 damage."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.concalAnyAndApplyFunction(playerId, Int.MAX_VALUE - 10) { card, b ->
            card.currentStrength = (card.currentStrength + if (b) -2 else 2).coerceAtLeast(1)
        }

    }
}


private object JoachimdeWett : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Silver, Tag.SPY)
    override val description = " Play the top non-Spying Bronze or Silver unit from your deck and boost it by 10."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.drawNPick1(playerId, 1, true, DrawNPickMMods.ToGame) { (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) && !it.ist(Tag.SPY) && !it.ist(Tag.SPECIAL) }
            ?: return
        card.playCardwithBattlecryAndEverything(playerId, PlayMod.FromDeck)
        card.boost(10)
    }
}


private object NilfgaardianGate : AbstractCard() {

    override var defaultStrenght = -1
    override val tags = listOf(Tag.Nilfgaard, Tag.SPECIAL, Tag.Silver)
    override val description = "Play a Bronze or Silver Officer from your deck and boost it by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { it.ist(Tag.Officer) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }?.boost(1)
    }
}


private object PeterSaarGwynleve : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Silver)
    override val description = "Reset an ally and Strengthen it by 3; or Reset an enemy and Weaken it by 3."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAny) ?: return
        card.reset()
        if (card.location().playerId == playerId)
            card.boost(3)
        else
            card.weaken(3)

    }
}


private object Serrit : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Nilfgaard, Tag.Witcher, Tag.Silver)
    override val description = "Deal 7 damage to an enemy or \n set a Revealed opposing unit to 1"


    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, description.split("\n"))[0]) {
            0 -> state.pickCardOnBattleFiled(playerId)?.dealDamage(7)
            1 -> state.applyActionToReveladCard(playerId, { card, b -> b && !card.ist(Tag.SPECIAL) }) { it.baseStrength = 1;it.currentStrength = 1 }
        }

    }
}


private object Sweers : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Silver)
    override val description = "Choose an enemy or a Revealed unit in your opponent's hand, then move all copies of it from from their deck to the graveyard."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val cardName = if (state.pickIf(playerId, "Reveled Card?")) {
            state.applyActionToReveladCard(playerId, { _, b -> b }) { it.name }
        } else {
            state.pickCardOnBattleFiled(playerId)?.name
        }
        state.actionOnDeck(playerId) { list, _ ->
            val cards = list.filter { it.name == cardName }
            list.removeAll(cards)
            return@actionOnDeck cards
        }.forEach { it.state = state;it.discard(1 - playerId) }
    }
}


private object TheGuardian : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Nilfgaard, Tag.Construct, Tag.Silver)
    override val description = "Add a Lesser Guardian< to the top of your opponent's deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        Card(LesserGuardian, state.getNextUUID(), state).addToDeck(1 - playerId, DeckLocation.TOP)
    }
}

object LesserGuardian : AbstractCard() {
    override val defaultStrenght = 6
    override val tags = listOf(Tag.Construct, Tag.Nilfgaard, Tag.TOKEN, Tag.Bronze)
    override val description = "Token"
}


private object Treason : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Nilfgaard, Tag.SPECIAL, Tag.Tactic, Tag.Silver)
    override val description = "Force 2 adjacent enemies to Duel each other."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mEnemy) ?: return
        val card2 = state.getCardByLocation(card.location().right()) ?: return
        state.duel(card, card2)
    }
}


private object Vanhemar : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Silver)
    override val description = "Spawn Frost, Clear Skies or Overdose."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(ClearSkies, Overdose, BitingFrost))
    }
}


private object Vreemde : AbstractCard() {

    override var defaultStrenght = 4
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Silver)
    override val description = "Create a Bronze Nilfgaardian Soldier"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.Bronze) && it.ist(Tag.Nilfgaard) && it.ist(Tag.Soldier) }
    }
}


private object Vrygheff : AbstractCard() {

    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Silver)
    override val description = "Play a Bronze Machine from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { it.ist(Tag.Machine) && it.ist(Tag.Bronze) }
    }
}


private object AlbaArmoredCavalry : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Whenever an ally appears, boost self by 1."

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.owner == thisCard.owner)
            thisCard.boost(1)
    }
}


private object AlbaPikeman : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Summon all copies of this unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonAllCopies(playerId, this, thisCard.location())
    }
}


private object AlbaSpearman : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Boost self by 1 whenever either player draws a card."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        TODO("Boost self by 1 whenever either player draws a card.")
    }

}


private object Alchemist : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Bronze)
    override val description = "Reveal 2 cards."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.revalCard(playerId, TargetMods.Any)
        state.revalCard(playerId, TargetMods.Any)

    }
}


private object Ambassador : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Nilfgaard, Tag.Bronze, Tag.SPY)
    override val description = "Spying. Boost an ally by 12. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.boost(12)

    }
}


private object Assasin : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Bronze, Tag.SPY)
    override val description = "Deal 10 damage to the unit to the left."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getCardByLocation(thisCard.location().left())?.dealDamage(10)
    }
}


private object CombatEngineer : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Nilfgaard, Tag.Support, Tag.Bronze, Tag.Crewd)
    override val description = "Boost an ally by 5. Crew.}"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.boost(5)
    }
}


private object DaerlanSoldier : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Whenever you Reveal this unit, play it on a random row and draw a card.}"

    override suspend fun onRevelad(state: State, thisCard: Card) {
        val x = thisCard.owner
        state.forceToPlay(x, { it.uuid == thisCard.uuid }, randomPlace = true)
        state.draw(x, 1)
    }
}


private object DeithwenArbalest : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Deal 3 damage to an enemy. If it is Spying deal 6 damage instead."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId) ?: return
        card.dealDamage(if (card.ist(Tag.SPY)) 6 else 3)
    }
}


private object Emissary : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Nilfgaard, Tag.Bronze, Tag.SPY)
    override val description = "Spying. Look at 2 random Bronze units from your deck, then play 1."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPick1(playerId, 2, top = false, mod = DrawNPickMMods.ToGame, filter = { it.ist(Tag.Bronze) && !it.ist(Tag.SPECIAL) })?.playCardwithBattlecryAndEverything(playerId, PlayMod.FromDeck)
    }
}


private object ExperimentalRemedy : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Bronze, Tag.Alchemy)
    override val description = "Resurrect a Bronze unit from your opponent's graveyard"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Enemy, filter = { it.ist(Tag.Bronze) && !it.ist(Tag.SPECIAL) })?.resurrect(playerId)
    }
}


private object FireScorpion : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Machine, Tag.Bronze)
    override val description = "Deal 5 damage to an enemy. Whenever you Reveal this unit, trigger its ability"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(5)
    }

    override suspend fun onRevelad(state: State, thisCard: Card) {
        state.pickCardOnBattleFiled(thisCard.owner)?.dealDamage(5)
    }
}


private object ImperaBrigade : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Boost self by 2 for each Spying Enemy. Whenever a Spying Enemy appears, Boost self by 2"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.boost(state.getAlCardsOnBattleFiled(1 - playerId).count { it.ist(Tag.SPY) } * 2)
    }

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.owner != thisCard.owner && card.ist(Tag.SPY))
            thisCard.boost(2)
    }
}


private object ImperaEnforcers : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Deal 2 damage to an enemy. For each Spying enemy that appears during your turn, deal 2 damage to an enemy on turn end. "
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.counter = 1
    }

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.owner != thisCard.owner && card.ist(Tag.SPY))
            thisCard.counter++
    }

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        repeat(thisCard.counter) {
            state.pickCardOnBattleFiled(playerId)?.dealDamage(2)
        }
        thisCard.counter = 0
    }

}


private object ImperialGolem : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Nilfgaard, Tag.Construct, Tag.Bronze)
    override val description = "Summon a copy of this unit whenever you Reveal a card in your opponent's hand.}}"
}
object ImperialGolemSpawner : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.SPAWNER)
    override val description = ""
    override suspend fun onCardReveled(state: State, card: Card, thisCard: Card, revelerId: Int) {
        state.summonRandom(revelerId){it.ac == ImperialGolem }
    }
}

private object Infiltrator : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Nilfgaard, Tag.Bronze)
    override val description = "Toggle a unit's Spying status"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAny) ?: return
        card.additionlTags.switch(Tag.SPY)

    }
}


private object MagneDivision : AbstractCard() {

    override var defaultStrenght = 3
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Play a random Bronze Item from your deck"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { it.ist(Tag.Item) && it.ist(Tag.Bronze) }
    }
}


private object Mangonel : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Machine, Tag.Bronze)
    override val description = "Deal 2 damage to a random enemy. Repeat this ability whenever you Reveal a card."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getRandomCardOnBattlefiled(playerId)?.dealDamage(2)
    }

    override suspend fun onCardReveled(state: State, card: Card, thisCard: Card, revelerId: Int) {
        if (revelerId == thisCard.owner)
            state.getRandomCardOnBattlefiled(thisCard.owner)?.dealDamage(2)
    }
}


private object MasterofDisguise : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Conceal 2 cards."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.concalAnyAndApplyFunction(playerId, 2) { _, _ -> }
    }
}


private object NauzicaaBrigade : AbstractCard() {

    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Damage a Spying Unit <u>by 7</u>. If it was Destroyed, Strengthen self by 4"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId) { it.ist(Tag.SPY) } ?: return
        if (card.dealDamage(7))
            thisCard.boost(4)
    }
}


private object NauzicaaSergeant : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Bronze)
    override val description = "Clear Hazards from its row and boost an ally or a Revealed unit by 3."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.applyWeather(thisCard.location(), null)
        if (state.pickIf(playerId, "Reveled Card")) {
            state.applyActionToReveladCard(playerId, { card, _ -> !card.ist(Tag.SPECIAL) }) { it.currentStrength += 3 }
        } else {
            state.pickCardOnBattleFiled(playerId, mAlly)?.boost(3)

        }

    }
}


private object NilfgaardianKnight : AbstractCard() {
    override var defaultStrenght = 12
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Reveal a random card in your hand. 2 Armor."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.revalCard(playerId, TargetMods.Ally) { list, random -> list.indices.random(random) }
        thisCard.giveArmour(2)
    }
}


private object Ointment : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Nilfgaard, Tag.SPECIAL, Tag.Item, Tag.Bronze, Tag.Alchemy)
    override val description = "Resurrect a Bronze unit with 5 power or less. <u>No longer a choice card"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId, TargetMods.Ally) { it.baseStrength <= 5 && !it.ist(Tag.SPECIAL) }
            ?: return
        card.resurrect(playerId)
    }
}


private object Recruit : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Play a random different Bronze Soldier from your deck. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { it.ac != thisCard.ac && it.ist(Tag.Bronze) && it.ist(Tag.Soldier) }
    }
}


private object RotTosser : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Machine, Tag.Bronze)
    override val description = "Spawn a Cow Carcass on an enemy row. " +
            "\nCow Carcass: 1 Strength, Token, Bronze. Spying. After 2 turns, destroy all the other Lowest units on the row and Banish self on turn end."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        CowCarcass.spawn(state, playerId, thisCard.location().opositeRow())
    }


}

object CowCarcass : AbstractCard() {
    override val defaultStrenght = 1
    override val tags = listOf(Tag.TOKEN, Tag.Bronze, Tag.SPY)
    override val description = " After 2 turns, destroy all the other Lowest units on the row and Banish self on turn end."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.counter = 2
    }

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter--
        if (thisCard.counter != 0)
            return
        val loation = thisCard.location()
        val lane = state.getRowByLocation(loation)
        lane.remove(thisCard)
        thisCard.banish(1 - loation.playerId)
        lane.allMin { it.currentStrength }.forEach { it.destroy() }
    }
}

private object Sentry : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Boost all copies of a Soldier by 2. <u>Added to the game<"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.Soldier) } ?: return
        state.boostAllWhereeverTheyAre(playerId, { it.ac == card.ac }, 2)


    }
}

private object YenneferDivination : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Aedirn, Tag.Gold)
    override val description = "Resurrect a Bronze or Silver Soldier from your opponent's graveyard"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId, TargetMods.Enemy) { it.ist(Tag.Soldier) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
            ?: return

        card.resurrect(playerId)
    }
}

private object SlaveDriver : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Nilfgaard, Tag.Officer, Tag.Bronze)
    override val description = "Set an ally's power to 1 and deal damage to an enemy by the amount of power lost"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        val dmg = card.currentStrength - 1
        card.setBaseStreanght(1)
        state.pickCardOnBattleFiled(playerId)?.dealDamage(dmg)
    }
}

private object SlaveHunter : AbstractCard() {

    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Charm a Bronze enemy with 3 power or less"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId) { it.ist(Tag.Bronze) && it.currentStrength <= 3 }?.charm()
    }
}


private object SlaveInfantry : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Spawn a Doomed default copy of this unit on your other rows."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (auxData.playMod == PlayMod.Spawned)
            return
        (0..2).filter { it != thisCard.location().rowId }.forEach { SlaveInfantry.spawn(state, playerId, forceedLocation = CardLocation(playerId, it, 0)).additionlTags.add(Tag.Doomed) }
    }

}


private object Spotter : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Boost self by the base power of a Revealed unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.boost(state.applyActionToReveladCard(playerId, { _, _ -> true }) {
            it.currentStrength
        } ?: return)
    }
}


private object StandardBearer : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Boost an ally by 2 whenever you play a Soldier"

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.ist(Tag.Soldier) && card.owner == thisCard.owner && playMod != PlayMod.Spawned) {
            state.getRandomCardOnBattlefiled(thisCard.owner, mAlly)?.boost(2)
        }
    }
}


private object VenendalElite : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Switch this unit's power with that of a Revealed unit."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val startBs = thisCard.baseStrength
        val startCs = thisCard.currentStrength
        val pair = state.applyActionToReveladCard(playerId, { card, _ -> !card.ist(Tag.SPECIAL) }) {
            val bs = it.baseStrength
            val cs = it.currentStrength
            it.baseStrength = startBs
            it.currentStrength = startCs
            return@applyActionToReveladCard Pair(bs, cs)
        } ?: return

        thisCard.setBaseStreanght(pair.first)
        thisCard.setCurrentStrenght(pair.second)
    }
}


private object VicovaroNovice : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Nilfgaard, Tag.Mage, Tag.Bronze)
    override val description = "Draw the top 2 Bronze Alchemy cards from your Deck.Play 1 and shuffle the other back</u>."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.drawNPick1(playerId, 2, top = true, mod = DrawNPickMMods.ToGame, filter = { it.ist(Tag.Alchemy) && it.ist(Tag.Bronze) })
            ?: return
        card.playCardwithBattlecryAndEverything(playerId, playMod = PlayMod.FromDeck)
    }
}

private object ViperWitcher : AbstractCard() {
    override val defaultStrenght = 5
    override val tags = listOf(Tag.Nilfgaard, Tag.Bronze, Tag.Witcher)
    override val description = "Deal 1 damage for each Alchemy card in your starting deck."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(state.getInfoAboutInitialDeck(playerId) { list, _ -> list.count { it.ist(Tag.Alchemy) } })
    }


}


