package walic.gwentBeta

import walic.pureEngine.*
import kotlin.math.ceil

val skeligeLeders = listOf(
    BranTuirseach,
    CrachAnCraite,
    EistTuirseach,
    HaraldtheCripple
)

val skeligeCard = listOf(
    BirnaBran,
    CerysFearless,
    Ermion,
    HjalmaranCraite,
    MadmanLugos,
    Ulfhedinn,
    Vabjorn,
    WildBoaroftheSea,
    ChampionofHov,
    DjengeFrett,
    GiantBoar,
    Gremist,
    Roar,
    HolgerBlackhand,
    JuttaanDimun,
    Skjall,
    SvanrigeTuirseach,
    Udalryk,
    Yoana,
    AnCraiteArmorsmith,
    AnCraiteBlacksmith,
    AnCraiteGreatsword,
    AnCraiteLongship,
    AnCraiteMarauder,
    AnCraiteRaider,
    AnCraiteWarcrier,
    AnCraiteWarrior,
    BrokvarHunter,
    DimunCorsair,
    DimunLightLongship,
    DimunWarship,
    DrummondShieldmaid,
    PriestessofFreya,
    SvalblodButcher,
    TuirseachArcher,
    TuirseachAxeman,
    TuirseachBearmaster,
    TuirseachSkirmisher,
    TuirseachVeteran,
    CerysanCraite,
    Hym,
    HeymaeySpearmaiden,
    Derran,
    DonaranHindar,
    StribogRunestone,
    HeymaeyProtector,
    HeymaeyHerbalist,
    HeymaeyFlaminica,
    DimunSmuggler,
    Restore,
    Olaf,
    DimunPirate,
    DimunPirateCaptain,
    DrummondQueensguard,
    DrummondWarmonger,
    AnCraiteWhaler,
    Kambi,
    SavageBear,
    Sigrdrifa,
    HeymaeySkald,
    BlueboyLugos,
    Morkvarg,
    Coral
)
val calans = listOf(Tag.Heymaey, Tag.Tuirseach, Tag.AnCraite, Tag.Dimun, Tag.Brokvar, Tag.Drummond)

private object BranTuirseach : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skelige, Tag.Leader)
    override val description = "Discard up to 3 cards from your deck and Strengthen them by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPickM(playerId, -1, 3, false, DrawNPickMMods.ToGame).forEach { it.discard(playerId); it.strengthen(1) }

    }
}


private object CrachAnCraite : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skelige, Tag.Leader)
    override val description = "Strengthen the Highest non-Spying Bronze or Silver unit in your deck by 2 and play it."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val cardUUid = state.actionOnDeck(playerId) { list, random ->
            val card = list.filter { !it.ist(Tag.SPY) && !it.ist(Tag.SPECIAL) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }.allMax { it.baseStrength }.randomOrNull(random)
                ?: return@actionOnDeck null
            card.currentStrength += 2
            return@actionOnDeck card.uuid
        } ?: return
        state.summonRandom(playerId) { it.uuid == cardUUid }

    }
}


private object EistTuirseach : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skelige, Tag.Leader, Tag.Tuirseach)
    override val description = "Spawn a Bronze Clan Tuirseach Soldier."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId, 1) { it.ist(Tag.Bronze) && it.ist(Tag.Tuirseach) }
    }
}


private object HaraldtheCripple : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Leader)
    override val description = "Split 10 damage randomly between enemies on the opposite row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val opositeRow = state.getRowByLocation(thisCard.location().opositeRow()).toMutableList()
        repeat(10) {
            opositeRow.randomOrNull(state.getRng())?.dealDamage(1)
        }
    }
}


private object BirnaBran : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Tuirseach, Tag.Officer, Tag.Gold)
    override val description = "Apply Skellige Storm to an enemy row"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = thisCard.location().opositeRow()
        state.applyWeather(row, SkelligeStormWether())
    }
}


private object CerysanCraite : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Officer, Tag.Gold)
    override val description = "When 4 units are Resurrected while this unit is in the graveyard, Resurrect it."

    override suspend fun onPutInGraviard(state: State, thisCard: Card) {
        thisCard.counter = 4
    }

    override suspend fun onCardAppearInGraviard(state: State, playerId: Int, card: Card, cardPlayerId: Int, playMod: PlayMod, thisCard: Card) {
        if (playMod == PlayMod.Resurected) {
            thisCard.counter--
        }
        if (thisCard.counter == 0) {
            thisCard.resurrect(playerId)
        }

    }
}


private object CerysFearless : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Officer, Tag.Gold)
    override val description = "Resurrect the next unit your Discard"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.counter = 1
    }

    override suspend fun onDiscarded(state: State, thisCard: Card) {
        if (!thisCard.ist(Tag.SPECIAL) && thisCard.owner == thisCard.owner && thisCard.counter > 0) {
            thisCard.resurrect(thisCard.owner)
            thisCard.counter--
        }
    }
}


private object Coral : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skelige, Tag.Mage, Tag.Gold)
    override val description = "Transform a Bronze or Silver unit into a Jade Figurine."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId){it.ist(Tag.Bronze)||it.ist(Tag.Silver)}?.transform(JadeFigurine)
    }

}


private object Ermion : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Gold)
    override val description = "Draw 2 cards, then Discard 2 cards.}"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.draw(playerId, 2)
        state.pickFromOwnHand(playerId, purpurs = "Discard")?.first?.discard(playerId)
        state.pickFromOwnHand(playerId, purpurs = "Discard")?.first?.discard(playerId)
    }
}


private object HjalmaranCraite : AbstractCard() {
    override var defaultStrenght = 16
    override val tags = listOf(Tag.Skelige, Tag.Officer, Tag.Gold, Tag.AnCraite)
    override val description = "Spawn the Lord of Undvik on the opposite row.Lord of Undvik: 5 Strength, Ogroid, Token, Silver. Spying. Deathwish: Boost enemy Hjalmars by 10"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location().opositeRow()
        LordOfUndvik.spawn(state, playerId, location)

    }


}

object LordOfUndvik : AbstractCard() {
    override val defaultStrenght = 5
    override val tags = listOf(Tag.Ogroid, Tag.TOKEN, Tag.Silver, Tag.SPY)
    override val description = "Token"
    override suspend fun onDeath(state: State, thisCard: Card) {
        state.getAlCardsOnBattleFiled(1 - thisCard.owner).firstOrNull { it.ac == HjalmaranCraite }?.boost(10)
    }

}

private object Hym : AbstractCard() {

    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Gold)
    override val description = "Choose One: Play a Bronze or Silver Cursed unit from your deck; or Create a Silver unit from your opponent's deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, listOf("Play a Bronze or Silver Cursed unit from your deck", "Create a Silver unit from your opponent's starting deck"))[0]) {
            0 -> state.summon(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver) && it.ist(Tag.Cursed)) }
            1 -> state.createCardFromEnemyStartingDeck(playerId) { it.ist(Tag.Cursed) && it.ist(Tag.Silver) }?.spawn(state, playerId)
        }
    }
}


private object Kambi : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Skelige, Tag.Gold, Tag.SPY)
    override val description = """ Deathwish: Spawn Hemdall. <u>Spying
*Hemdall: 20 Strength,  Token, Gold. Destroy all units and clear all Boons and Hazards.
"""

    override suspend fun onDeath(state: State, thisCard: Card) {
        Hemdall.spawn(state, thisCard.owner, forceedLocation = thisCard.location())
    }


}

object Hemdall : AbstractCard() {
    override var defaultStrenght = 20
    override val tags = listOf(Tag.Skelige, Tag.Gold, Tag.TOKEN)
    override val description = "Destroy all units and clear all Boons and Hazards."
    override suspend fun onDeath(state: State, thisCard: Card) {
        val playerID = thisCard.owner
        state.getAlCardsOnBattleFiled(playerID).filter { it.name != thisCard.name }.forEach { it.destroy() }
        (0..2).forEach { state.applyWeather(CardLocation(playerID, it), null) }
    }

}

private object MadmanLugos : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Drummond, Tag.Officer, Tag.Gold)
    override val description = "Discard a Bronze Unit from your Deck and Damage a Unit by the Discarded Unit's base Power."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.drawNPick1(playerId, -1, false, DrawNPickMMods.ToGame) { it.ist(Tag.Bronze) } ?: return
        state.pickCardOnBattleFiled(playerId)?.dealDamage(card.baseStrength)
        card.discard(playerId)
    }
}


private object Olaf : AbstractCard() {
    override var defaultStrenght = 20
    override val tags = listOf(Tag.Skelige, Tag.Beast, Tag.Cursed, Tag.Gold)
    override val description = "Deal 10 damage to self. Reduce the damage inflicted by 2 for each Beast you played this match."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.dealDamage((10 - state.cardPlayedHistory.count { it.first.ist(Tag.Beast) && it.second == playerId } * 2).coerceAtLeast(0))


    }
}


private object Ulfhedinn : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Beast, Tag.Cursed, Tag.Gold)
    override val description = "Deal 1 damage to all enemies. If the enemies were damaged, deal 2 damage instead"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(1 - playerId).forEach {
            it.dealDamage(if (it.isDamaged()) 2 else 1)
        }
    }
}


private object Vabjorn : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Cultist, Tag.Gold)
    override val description = "Deal 2 damage. If the unit was already damaged , destroy it instead."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId) ?: return
        if (card.isDamaged())
            card.destroy()
        else
            card.dealDamage(2)
    }
}


private object WildBoaroftheSea : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Machine, Tag.Gold)
    override val description = "On turn end, Strengthen the unit to the left by 1, then deal 1 damage to the unit to the right. 5 Armor."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(5)
    }

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        val location = thisCard.location()
        state.getCardByLocation(location.right())?.dealDamage(1) ?: return
        state.getCardByLocation(location.left())?.strengthen(1)

    }
}


private object BlueboyLugos : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Drummond, Tag.Soldier, Tag.Silver, Tag.Cursed)
    override val description = """*Spawn a Spectral Whale on an enemy row.
        *Spectral Whale: 3 Strength, <u>Cursed</u>, <u>Token</u>, Silver. Move to a random row and deal 1 damage to all units on it on turn <u>end</u>."""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        SpectralWhale.spawn(state, playerId, thisCard.location())
    }


}

object SpectralWhale : AbstractCard() {

    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.TOKEN, Tag.Silver)
    override val description = "Move to a random row and deal 1 damage to all units on it on turn."

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        thisCard.move(state.getRng().nextInt(0, 3))
        state.getRowByLocation(thisCard.location()).toMutableList().forEach { it.dealDamage(1) }
    }
}

private object ChampionofHov : AbstractCard() {

    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Ogroid, Tag.Silver)
    override val description = "Duel an enemy."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.duel(thisCard, state.pickCardOnBattleFiled(playerId))
    }
}


private object Derran : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Tuirseach, Tag.Silver)
    override val description = "Whenever an enemy is damaged, boost this unit by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }

    override suspend fun onCardDamaged(state: State, card: Card, thisCard: Card) {
        if (thisCard.owner != card.owner()) {
            thisCard.boost(1)
        }

    }
}


private object DjengeFrett : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skelige, Tag.Dimun, Tag.Soldier, Tag.Silver)
    override val description = "Deal 1 damage to 2 allies and Strengthen self by 2 for each"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
            card.dealDamage(1)
            thisCard.boost(2)
        }
    }
}


private object DonaranHindar : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Officer, Tag.Silver)
    override val description = "Toggle a unit's Lock status, then move a Bronze unit from your opponent's graveyard to yours."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.toggleLock()
        val card = state.pickFromGraviard(playerId, TargetMods.Enemy) { it.ist(Tag.Bronze) && !it.ist(Tag.SPECIAL) }
            ?: return
        state.players[playerId].graveyard.add(card)

    }
}


private object GiantBoar : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Beast, Tag.Silver)
    override val description = "Destroy a random ally, then boost self by 10."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        (state.getAlCardsOnBattleFiled(playerId).filter { it != thisCard }.randomOrNull(state.getRng())
            ?: return).destroy()
        thisCard.boost(10)

    }
}


private object Gremist : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Silver)
    override val description = "Spawn Rain, Clear Skies or Roar."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(TorrentialRain, ClearSkies, Roar))
    }

}

private object Roar : AbstractCard() {
    override val defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Bronze)
    override val description = "Destroy an Ally. Spawn a Bear. "
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.destroy() ?: return
        Bear.spawn(state, playerId)
    }

}

//private object HaraldHoundsnout:AbstractCard(){
//    //TODO zrobić
//    override var deafulStrenght = 6
//    override val tags = listOf(Tag.Skelige,Tag.Cursed,Tag.Tordarroch,Tag.Silver,Tag.Cursed,Tag.Token,Tag.Cursed,Tag.Token,Tag.Cursed,Tag.Token,Tag.Token.}}
//
//
//)
//override val description ="""Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
//*6 Strength, Cursed, Tordarroch, Silver, Epic.
//*Spawn Wilfred, Wilhelm and Wilmar.
//*Wilfred: 1 Strength, Cursed, <u>Token</u>, Silver. Deathwish: Strengthen a random ally by 3. <u>Token</u>.
//*Wilhelm: 1 Strength, Cursed, <u>Token</u>, Silver. Deathwish: Deal 1 damage to all enemies on the opposite row. <u>Token</u>.
//*Wilmar: 1 Strength, Cursed, <u>Token</u>, Silver. Deathwish: If it's your opponent's turn, <u>Spawn a Bear</u> on the opposite row. <u>Spying, Token</u>.}}
//
//
//"""
//override fun onEntry(state: State,playerId: Int, location: State.CardLocation) {
//}
//}


private object HolgerBlackhand : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Dimun, Tag.Officer, Tag.Silver)
    override val description = "Deal 6 damage. If the unit was destroyed, Strengthen the Highest unit in your graveyard by 3."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickCardOnBattleFiled(playerId)?.dealDamage(6) == true) {
            state.players[playerId].graveyard.allMax { it.baseStrength }.randomOrNull(state.getRng())?.strengthen(3)
        }

    }
}


private object JuttaanDimun : AbstractCard() {
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Silver)
    override val description = "Deal 1 damage to self."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.dealDamage(1)
    }
}


private object Morkvarg : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Beast, Tag.Cursed, Tag.Silver)
    override val description = "Whenever this unit enter the graveyard, Resurrect it and Weaken it by half."

    override suspend fun onPutInGraviard(state: State, thisCard: Card) {
        thisCard.weaken(ceil(thisCard.baseStrength / 2.0).toInt())
        thisCard.resurrect(thisCard.owner)
    }
}


private object Restore : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skelige, Tag.SPECIAL, Tag.Spell, Tag.Silver)
    override val description = "Return a Bronze or Silver Skellige unit from your graveyard to your hand, add the Doomed category to it, and set its base power to 8. Then play a card."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId, TargetMods.Ally) { it.ist(Tag.Skelige) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && !it.ist(Tag.SPECIAL) }
            ?: return
        if (!card.ist(Tag.Doomed))
            card.additionlTags.add(Tag.Doomed)
        card.setBaseStreanght(8)
        card.addToHand(playerId)
        state.forceToPlay(playerId)
    }
}


private object Sigrdrifa : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Doomed, Tag.Silver)
    override val description = "Resurrect a Bronze or Silver Clan unit."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Ally) { card -> calans.any { card.ist(it) } && (card.ist(Tag.Bronze) || card.ist(Tag.Silver)) }?.resurrect(playerId)
    }
}


private object Skjall : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Heymaey, Tag.Silver)
    override val description = "Play a random Bronze or Silver Cursed Unit from your Deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { it.ist(Tag.Cursed) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
    }
}


private object StribogRunestone : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skelige, Tag.SPECIAL, Tag.Item, Tag.Silver)
    override val description = "Create a Bronze or Silver Skellige card."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.Silver) || it.ist(Tag.Skelige) }
    }
}


private object SvanrigeTuirseach : AbstractCard() {

    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Officer, Tag.Silver)
    override val description = "Draw a card, then Discard a card."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.draw(playerId, 1)
        state.pickFromOwnHand(playerId, purpurs = "Discard")?.first?.discard(playerId)
    }
}


private object Udalryk : AbstractCard() {
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Agent, Tag.Silver, Tag.SINGLEUSE)
    override val description = "Look at 2 cards from your deck. Draw one and Discard the other"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPickM(playerId, 2, 1, top = false, mod = DrawNPickMMods.ToHandGetRest).forEach { it.discard(playerId) }
    }
}


private object Yoana : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Silver)
    override val description = "Heal an ally, then boost it by the amount Healed"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        val health = card.currentStrength
        card.heal()
        card.boost(card.currentStrength - health)

    }
}


private object AnCraiteArmorsmith : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze)
    override val description = "Heal 2 allies and and give them 3 Armor"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
            card.heal()
            card.giveArmour(3)
        }
    }
}


private object AnCraiteBlacksmith : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze)
    override val description = "Strengthen an ally by 2 and give it 2 Armor"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        card.strengthen(2)
        card.giveArmour(2)
    }
}


private object AnCraiteGreatsword : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Every 2 turns, if damaged, Heal self and Strengthen by 2 on turn start"

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter += 1
        if (thisCard.counter == 2) {
            thisCard.counter = 0
            if (thisCard.isDamaged()) {
                thisCard.heal()
                thisCard.strengthen(2)
            }
        }
    }

}


private object AnCraiteLongship : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Machine, Tag.Bronze)
    override val description = "Deal 2 damage to a random enemy. Repeat this ability whenever you Discard a card."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getRandomCardOnBattlefiled(playerId, mEnemy)?.dealDamage(2)
    }

    override suspend fun onCardDiscared(state: State, card: Card, thisCard: Card) {
        state.getRandomCardOnBattlefiled(thisCard.owner)?.dealDamage(2)
    }

}


private object AnCraiteMarauder : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Deal 4 damage. If Resurrected, deal 6 damage instead."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(if (auxData.playMod == PlayMod.Resurected) 6 else 4)
    }
}


private object AnCraiteRaider : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Whenever you Discard this unit, Resurrect it"
    override suspend fun onDiscarded(state: State, thisCard: Card) {
        thisCard.resurrect(thisCard.owner)
    }


}


private object AnCraiteWarcrier : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze)
    override val description = "Boost an ally by half its power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        card.boost(card.currentStrength / 2)
    }
}


private object AnCraiteWarrior : AbstractCard() {
    override var defaultStrenght = 12
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Deal 1 damage to self"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.dealDamage(1)
    }
}


private object AnCraiteWhaler : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Machine, Tag.Bronze)
    override val description = "Move an enemy to the opposite row, then deal damage equal to the number of units on that row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = thisCard.location().opositeRow()
        val card = state.pickCardOnBattleFiled(playerId) ?: return
        card.move(row)
        card.dealDamage(state.getRowByLocation(row).count() - 1)
    }
}


private object BrokvarHunter : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze, Tag.Brokvar)
    override val description = "Deal 5 damage."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(5)
    }
}


private object DimunCorsair : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Doomed, Tag.Bronze, Tag.Dimun)
    override val description = "Resurrect a Bronze Machine"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId) { it.ist(Tag.Machine) && it.ist(Tag.Bronze) } ?: return
        card.resurrect(playerId)
    }
}


private object DimunLightLongship : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Machine, Tag.Bronze, Tag.Dimun)
    override val description = "On turn end, deal 1 damage to the unit to the right, then boost self by 2"
    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        state.getCardByLocation(thisCard.location().right())?.dealDamage(1) ?: return
        thisCard.boost(2)
    }

}


private object DimunPirate : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze, Tag.Dimun)
    override val description = "Discard all copies of this unit from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        while (null != state.discardFromDeck(playerId) { it.name == thisCard.name }) {
            //aby nie nazekał
        }
    }
}


private object DimunPirateCaptain : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Skelige, Tag.Officer, Tag.Bronze, Tag.Dimun)
    override val description = "Play a different Bronze Dimun unit from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) {  it.ist(Tag.Bronze) && it.ist(Tag.Dimun) && it.ac != this }
    }
}


private object DimunSmuggler : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze, Tag.Dimun)
    override val description = "Return a Bronze unit from your graveyard to your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Ally) { it.ist(Tag.Bronze) && !it.ist(Tag.SPECIAL) }?.addToDeck(playerId)

    }
}


private object DimunWarship : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Machine, Tag.Bronze, Tag.Dimun)
    override val description = "Deal 1 damage to the same unit 4 times"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId)
        repeat(4)
        {
            card?.dealDamage(1)
        }
    }
}


private object DrummondQueensguard : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Resurrect all your copies of this unit."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].graveyard.filter { it.name == thisCard.name }.forEach {
            it.resurrect(playerId)
        }
    }
}


private object DrummondShieldmaid : AbstractCard() {

    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Summon all copies of this unit."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonAllCopies(playerId, this, thisCard.location())
    }
}


private object DrummondWarmonger : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Discard a Bronze card from your deck"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.discardFromDeck(playerId) { it.ist(Tag.Bronze) }

    }
}


private object HeymaeyFlaminica : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze, Tag.Heymaey)
    override val description = "Clear Hazards from the row and move 2 allies to it"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mAlly)
        state.applyWeather(row, null)
        repeat(2) {
            state.pickCardOnBattleFiled(playerId, mAlly)?.move(row.rowId)
        }
    }
}


private object HeymaeyHerbalist : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze)
    override val description = "Play a random Bronze Organic or Hazard card from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { it: Card -> it.ist(Tag.Bronze) && (it.ist(Tag.Organic) && it.ist(Tag.Hazard)) }
    }
}


private object HeymaeyProtector : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Play a Bronze Item from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) {  it.ist(Tag.Bronze) && it.ist(Tag.Item) }
    }
}


private object HeymaeySkald : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze, Tag.Heymaey)
    override val description = "Boost all allies from a Clan of your choice by 1"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val clan = calans[state.pickNofM(playerId, 1, calans.map { it.toString() })[0]]
        state.getAlCardsOnBattleFiled(playerId).filter { it.ist(clan) }.forEach { it.boost(1) }
    }
}


private object HeymaeySpearmaiden : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze)
    override val description = "Deal 1 damage to a Machine or Soldier ally, then play a copy of it from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.Silver) || it.ist(Tag.Machine) } ?: return
        card.dealDamage(1)
        state.summonRandom(playerId,card.ac)

    }
}


private object PriestessofFreya : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Skelige, Tag.Heymaey, Tag.Support, Tag.Doomed, Tag.Bronze)
    override val description = "Resurrect a Bronze Soldier"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId) { it.ist(Tag.Bronze) && it.ist(Tag.Soldier) }?.resurrect(playerId)
    }
}


private object SavageBear : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Beast, Tag.Cursed, Tag.Bronze)
    override val description = "Whenever a unit is played from either hand on your opponent's side, deal 1 damage to it."


    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.owner != thisCard.owner)
            card.dealDamage(1)
    }
}


private object SvalblodButcher : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Soldier, Tag.Cultist, Tag.Bronze)
    override val description = "Boost self by 1 for each damaged or Cursed ally."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.boost(state.getAlCardsOnBattleFiled(playerId).count { it.ist(Tag.Cursed) && it.isDamaged() })
    }
}


private object SvalblodRavager : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Cursed, Tag.Soldier, Tag.Cultist, Tag.Bronze, Tag.Beast, Tag.Cursed, Tag.Cultist)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*8 Strength, Cursed, Soldier, <u>Cultist</u>, Bronze, Rare.
*Whenever this unit is damaged or Weakened(not yet), transform into a Raging Bear.
*Raging Bear: 12 Strength, Beast, Cursed, <u>Cultist</u>, Bronze. No ability. <u>Note: Renamed Svalblod to Cultist</u>.}}


*"""



    override suspend fun onDamaged(state: State, thisCard: Card) {

    }
}


private object TuirseachArcher : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Deal 1 damage to 3 units."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) {
            state.pickCardOnBattleFiled(playerId, mAny)?.dealDamage(1)
        }
    }
}


private object TuirseachAxeman : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze, Tag.Tuirseach)
    override val description = "*Whenever an enemy on the opposite row is damaged, boost self by 1. 2 Armor."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(2)
    }

    override suspend fun onCardDamaged(state: State, card: Card, thisCard: Card) {
        val cardRow = card.location()
        val thisRow = thisCard.location()
        if (cardRow.playerId != thisRow.playerId && cardRow.rowId == thisRow.rowId)
            thisCard.boost(1)
    }
}


private object TuirseachBearmaster : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze, Tag.Beast, Tag.Cursed)
    override val description = "Spawn a Bear. Bear: 11 Strength, Beast, Cursed, Bronze. No ability."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        Bear.spawn(state, playerId)
    }
}


private object TuirseachSkirmisher : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skelige, Tag.Soldier, Tag.Bronze)
    override val description = "Whenever this unit is Resurrected, Strengthen it by 3."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (auxData.playMod == PlayMod.Resurected)
            thisCard.strengthen(3)
    }
}


private object TuirseachVeteran : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skelige, Tag.Support, Tag.Bronze, Tag.Tuirseach)
    override val description = "Strengthen all your other Clan Tuirseach units in hand, deck, and on board by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId).filter { it != thisCard }.filter { it.ist(Tag.Tuirseach) }.forEach { it.strengthen(1) }
        state.boostAllInDeckAndHand(playerId, 1) { it.ist(Tag.Tuirseach) }
    }
}

