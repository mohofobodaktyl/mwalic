package walic.gwentBeta

import walic.pureEngine.*


class BrouverHoog : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Leader, Tag.Dwarf)
    override val description = "Play a non-SpyingSilver unit or a Bronze Dwarf from your deck"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { !it.ist(Tag.SPY) && ((it.ist(Tag.Silver) && !it.ist(Tag.SPECIAL)) || (it.ist(Tag.Bronze) && it.ist(Tag.Dwarf))) }


    }
}


class Eithne : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skojatel, Tag.Leader, Tag.Dryad)
    override val description = "Resurrect a Bronze or Silver special card"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId) { it.ist(Tag.SPECIAL) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) } ?: return
        card.resurrect(playerId)
    }
}


class Filavandrel : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Leader, Tag.Elf)
    override val description = "Create a Silver special card"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.SPECIAL) && it.ist(Tag.Silver) }
    }
}


class FrancescaFindabair : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Leader, Tag.Mage, Tag.Elf)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Francesca Findabair</u>, 7 Strength, Leader, Mage, Elf, Legendary.
*Swap a card with one of your choice and boost it by 3.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Aglasi : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Dryad, Tag.Gold)
    override val description = "Resurrect a Bronze or Silver special card from your opponent's graveyard, <u>then Banish it"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Enemy) { it.ist(Tag.Bronze) && (it.ist(Tag.Spell) || it.ist(Tag.SPECIAL)) }?.apply {
            playCardwithBattlecryAndEverything(playerId, PlayMod.Resurected)
            banish(playerId)
        }

    }
}


class Iorveth : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Gold)
    override val description = "Deal <u>8 damage</u> to an enemy. If the unit was destroyed, boost all Elves in your hand by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickCardOnBattleFiled(playerId)?.dealDamage(8) ?: return) {
            state.boostAllInHand(playerId, 1) { it.ist(Tag.Elf) }
        }
    }
}


class IorvethMeditation : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Gold)
    override val description = "Force 2 <u>enemies on the same row</u> to Duel each other."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val c1 = state.pickCardOnBattleFiled(playerId, mEnemy) ?: return
        val c2 = state.pickCardOnBattleFiled(playerId, mEnemy) { it != c1 } ?: return
        state.duel(c1,c2)

    }
}


class IsengrimFaoiltiarna : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Gold)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Isengrim Faoiltiarna</u>, 7 Strength, Elf, Officer, Gold, Legendary.
*Play a Bronze or Silver Ambush from your Deck.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {


    }
}


class IsengrimOutlaw : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Gold)
    override val description = """Patch|Jan 11, 2018|0.9.19 Hotfix.
*2 Strength, Elf, Officer, Gold, Legendary.
*Choose One: Play a Bronze or Silver special card from your deck; or Create a Silver Elf. <u>Removed CA Spies from Create pool</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class IthlinneAegli : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Mage, Tag.Gold)
    override val description = "Play a Bronze Spell, Boon or Hazard from your deck twice."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId){it.ist(Tag.Bronze)&& it.ist(Tag.Spell)}?.resurrect(playerId)
    }
}


class Milva : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Gold)
    override val description = "Return each player's Highest Bronze or Silver unit to their deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        (0..1).forEach { pid ->
            state.getAlCardsOnBattleFiled(pid).allMax { it.currentStrength }.randomOrNull(state.getRng())?.addToDeck(playerId)
        }
    }
}


class MorennForestChild : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Dryad, Tag.Gold)
    override val description = """Patch|Feb 28, 2018|0.9.22 Bear Season.
*6 Strength, Dryad, Gold, Legendary.
*Ambush: When your opponent plays a Bronze or Silver special card, flip over and cancel its ability.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Saesenthessis : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Aedirn, Tag.Draconid, Tag.Gold)
    override val description = "Boost self by 1 for each Dwarf ally and deal 1 damage to an enemy for each Elf ally."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.boost(state.getAlCardsOnBattleFiled(playerId).count { it.ist(Tag.Elf) })
        state.pickCardOnBattleFiled(playerId)?.dealDamage(state.getAlCardsOnBattleFiled(playerId).count { it.ist(Tag.Dwarf) })
    }
}


class Saskia : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Skojatel, Tag.Aedirn, Tag.Draconid, Tag.Gold)
    override val description = "Swap 2 cards with 2 Bronze cards. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.swap(playerId,deckFilter = {it.ist(Tag.Bronze)})
    }
}


class Schirru : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Gold)
    override val description = "Spawn</u> Scorch or <u>Epidemic</u>."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(Epidemic, Scorch))
    }
}


class ZoltanChivay : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Gold)
    override val description = "Choose 3 units. Strengthen allies by 2 and move them to this unit's row. Damage enemies by 2 and move them to the opposite row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickNCardsOnBattlefileD(playerId, mAny,3).forEach {
            if (it.owner==playerId) {
                it.boost(2)
                it.move(thisCard.location())
            }else
            {
                it.dealDamage(2)
                it.move(thisCard.location().opositeRow())
            }
        }
    }
}


class Aelirenn : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*6 Strength, <u>Agile</u>, Elf, Officer, Silver, Epic.
*When 5 Elven allies are on the board, Summon this unit.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class BarclayEls : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Officer, Tag.Silver)
    override val description = "Play a random Bronze or Silver Dwarf from your deck and Strengthen it by 3"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && it.ist(Tag.Dwarf) }?.strengthen(3)
    }
}


class Braenn : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Dryad, Tag.Silver)
    override val description = "Deal damage equal to this unit's power. If a unit was destroyed, boost all your Dryad and Ambush units by 1, wherever they are."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickCardOnBattleFiled(playerId)?.dealDamage(thisCard.currentStrength) == true){
            state.boostAllWhereeverTheyAre(playerId,{it.ist(Tag.Dryad)||it.ist(Tag.Ambush)},1)
        }
    }
}


class CiaranaepEasnillen : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Silver)
    override val description = "Toggle a unit's Lock status and move it to this unit's row on its side"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.apply {
            toggleLock()
            move(thisCard.location().opositeRow())
        }
    }
}


class DennisCranmer : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Officer, Tag.Silver)
    override val description = "Strengthen all your Dwarves by 1, wherever they are. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.boostAllWhereeverTheyAre(playerId,{it.ist(Tag.Dwarf)},1)
    }
}


class EibhearHattori : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Support, Tag.Doomed, Tag.Silver)
    override val description = "Resurrect a lower or equal Bronze or Silver Scoia'tael unit. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId){it.currentStrength<=thisCard.currentStrength}?.resurrect(playerId)
    }
}


class Eleyas : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>10 Strength</u>, Elf, Soldier, Silver, Epic.
*Whenever you draw this unit or return it to your deck, boost self by 2.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class IdaEmeanaepSivney : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Mage, Tag.Silver)
    override val description = "Spawn Impenetrable Fog, Clear Skies or Alzur's Thunder."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(ImpenetrableFog, ClearSkies, AlzursThunder))
    }
}


class MahakamHorn : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Item, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*Special, Item, Silver, Epic.
*Choose One: Create a Bronze or Silver Dwarf; or Strengthen a unit by 7.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Malena : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>7 Strength</u>, Elf, Silver, Epic.
*<u>Ambush: After 2 turns, flip over and Charm the Highest Bronze or Silver enemy with 5 power or less</u>. <u>Removed Armor</u>. <u>Removed Double Agent Tag</u>. <u>No Loyal only</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Milaen : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Silver)
    override val description = "Deal 6 damage to the units at the end of a row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class MoranaRunestone : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Item, Tag.Silver)
    override val description = "Create a <u>Bronze or</u> Silver Scoia'tael card"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

    }
}


class Morenn : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Dryad, Tag.Silver)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>8 Strength</u>, Dryad, Silver, Epic.
*Ambush: When a unit is played from either hand on your opponent's side, flip over and deal <u>7 damage</u> to that unit.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId){(it.ist(Tag.Bronze)||it.ist(Tag.Silver)&&it.ist(Tag.Skojatel))}
    }
}


class NaturesGift : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Spell, Tag.Silver)
    override val description = """Patch|Aug 29, 2017|0.9.10 Gold Immunity Major Update.
*Event, Special, <u>Spell</u>, Silver, Epic.
*Play a Bronze or Silver Special card from your Deck. Shuffle the others back.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

    }
}


class PaulieDahlberg : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skojatel, Tag.Support, Tag.Dwarf, Tag.Doomed, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*3 Strength, Support, Dwarf, Doomed, Silver, Epic.
*Resurrect a non-Support Bronze Dwarf. Doomed.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class PavkoGale : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Silver)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*5 Strength, Soldier, Silver, Epic.
*Play a Bronze or Silver Item from your deck. <u>No longer random</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class PitTrap : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Item, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*Special, Item, Silver, Epic.
*Apply a Hazard to an enemy row that deals 3 damage to units on contact.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class SheldonSkaggs : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Officer, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>9 Strength</u>, Dwarf, Officer, Silver, Epic.
*Move all allies on this unit's row to random rows and boost self by 1 for each.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Toruviel : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Silver)
    override val description = """Patch|Aug 29, 2017|0.9.10 Gold Immunity Major Update.
*6 Strength, Agile, Elf, <u>Officer</u>, Loyal, Silver, Epic.
*Ambush: When your opponent passes, turn this Unit over and Boost 2 Units on each side by 2.
*<u>General change; Ambush is no longer a tag</u>. <u>Bug Fix: Fixed an issue with Toruviel and Vrihedd Sappers not counting as Elf triggers when revealed</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Yaevinn : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Agent, Tag.Silver)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*13 Strength, Elf, Agent, Silver, Epic.
*Spying. <u>Single-Use</u>. Draw a special card and a unit. Keep one and return the other to your deck. <u>Replaced Doomed Tag with a new Single-Use keyword in ability section</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class YarpenZigrin : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Soldier, Tag.Silver)
    override val description = "Resilience. Whenever a Dwarf ally appears, boost self by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.togleResilience(thisCard)
    }

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.ist(Tag.Dwarf))
            thisCard.boost(1)
    }
}


class BlueMountainElite : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = "Summon all copies of this unit. Whenever this unit moves, boost self by 2."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonAllCopies(playerId,this,thisCard.location())
    }

    override suspend fun onMoved(state: State, thisCard: Card) {
        thisCard.boost(2)
    }
}


class DolBlathannaArcher : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Dol Blathanna Archer</u>, 7 Strength, Soldier, Elf, Bronze, Common.
*Deal 3 damage, then deal 1 damage.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class DolBlathannaBomber : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Dol Blathanna Bomber</u>, 6 Strength, Soldier, Elf, Bronze, Rare.
*Spawn an Incinerating Trap on an enemy row.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class DolBlathannaBowman : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Dol Blathanna Bowman</u>, Soldier, Elf, Bronze, Common.
*Deal 2 damage to an enemy. Whenever an enemy moves, deal 2 damage to it. Whenever this unit moves, deal 2 damage to a random enemy.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class DolBlathannaSentry : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Dol Blathanna Sentry</u>, 2 Strength, Soldier, Elf, Bronze, Rare.
*If in hand, deck or on board, boost self by 1 whenever you play a special card.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class DwarvenAgitator : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Skojatel, Tag.Support, Tag.Dwarf, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>1 Strength</u>, Support, Dwarf, Bronze, Rare.
*Spawn a default copy of a random different Bronze Dwarf from your Deck.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class DwarvenMercenary : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Dwarf, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>8 Strength</u>, Soldier, Dwarf, Bronze, Common.
*Move a unit to this row on its side. If it's an ally, boost it <u>by 3</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class DwarvenSkirmisher : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Dwarf, Tag.Bronze)
    override val description = "Deal 3 damage to an enemy. If the unit was not destroyed, <u>boost self</u> by 3."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (!(state.pickCardOnBattleFiled(playerId)?.dealDamage(3) ?:return))
            thisCard.boost(3)

    }
}


class ElvenMercenary : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = "Look at 2 random Bronze special cards from your deck, then play 1. <u>Improved Premium version"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPick1(playerId,2,false, DrawNPickMMods.ToGame){it.ist(Tag.Bronze)&&it.ist(Tag.SPECIAL)}?.playCardwithBattlecryAndEverything(playerId,PlayMod.FromDeck)
    }
}


class ElvenScout : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Elf, Tag.Bronze)
    override val description = "Swap a card<"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.swap(playerId)
    }
}


class ElvenSwordmaster : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = "Deal damage equal to this unit's power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(thisCard.currentStrength)
    }
}


class Farseer : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Mage, Tag.Bronze)
    override val description = """Patch|Sep 5, 2017|0.9.10.33 September Hotfix.
*8 Strength, Agile, Elf, Mage, Loyal, Bronze, Rare.
*If a different Ally or Unit in your Hand is Boosted during your turn, Boost self by 2 at the end of the turn.
*<u>Note: Will no longer Boost self when Units in your Deck are Boosted</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class HawkerHealer : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Support, Tag.Bronze)
    override val description = "Boost 2 Allies by 3."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            state.pickCardOnBattleFiled(playerId, mAlly)?.boost(3)
        }
    }
}


class HawkerSmuggler : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Support, Tag.Bronze)
    override val description = "Whenever an enemy appears, boost self by 1"

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (thisCard.owner != card.owner)
            thisCard.boost(1)
    }
}


class HawkerSupport : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Support, Tag.Bronze)
    override val description = "Boost a unit in your hand by 3"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.boostRandomInHand(playerId,1,3)

    }
}


class IncineratingTrap : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Skojatel, Tag.SPECIAL, Tag.Machine, Tag.TOKEN)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>Incinerating Trap</u>, Machine, <u>Token</u>, Bronze.
*Damage all units on its row by 2 on turn end. <u>Token</u>. <u>Removed Doomed from ability section</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class MahakamDefender : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Soldier, Tag.Bronze)
    override val description = "Resilient"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.togleResilience(thisCard)
    }

}


class MahakamGuard : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Dwarf, Tag.Bronze)
    override val description = "Boost an ally by <u>7<"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.boost(7)
    }
}


class MahakamMarauder : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Soldier, Tag.Bronze)
    override val description = """Patch|Sep 29, 2017|0.9.11 Agile Update.
*<u>7 Strength</u>, Agile, Dwarf, Soldier, Loyal, Bronze, Rare.
*Whenever this Unit is Boosted, Damaged, Strengthened or Weakened, Boost it by 2.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class MahakamVolunteers : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skojatel, Tag.Dwarf, Tag.Soldier, Tag.Bronze)
    override val description = "Summon all copies of this unit."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonAllCopies(playerId, this, thisCard.location())
    }
}


class Panther : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Skojatel, Tag.Beast, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*4 Strength, Beast, Bronze, Rare.
*Deal 7 damage to an enemy on a row with less than 4 units.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Pyrotechnician : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skojatel, Tag.Soldier, Tag.Dwarf, Tag.Bronze)
    override val description = "Deal 3 damage to a random enemy on each row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        (0..3).forEach {
            state.getRandomCardOnBattlefiled(playerId, maskOnlyRow(playerId, CardLocation(1 - playerId, it)))?.dealDamage(3)
        }
    }
}


class Sage : AbstractCard() {

    override var defaultStrenght = 2
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Mage, Tag.Bronze)
    override val description = "Resurrect a Bronze Alchemy or Spell card, then Banish it."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Ally) { it.ist(Tag.Bronze) && (it.ist(Tag.Spell) || it.ist(Tag.Alchemy)) }?.apply {
            playCardwithBattlecryAndEverything(playerId, PlayMod.Resurected)
            banish(playerId)
        }
    }
}


class VriheddBrigade : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>9 Strength</u>, Elf, Soldier, Bronze, Rare.
*Clear Hazards from its row and move a unit to this row on its side.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class VriheddDragoon : AbstractCard() {

    override var defaultStrenght = 8
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = "Boost a random loyal unit in your hand by 1 on turn <u>end<"

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        state.boostRandomInHand(playerId, 1, 1)
    }
}


class VriheddNeophyte : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = "Boost 2 random units in your hand by 1"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.boostRandomInHand(playerId, 2, 1)
    }
}


class VriheddOfficer : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Officer, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>5 Strength</u>, Elf, Officer, Bronze, Common.
*Swap a card and boost self by its base power. <u>No longer boosts self by 5 if a swapped card was a special</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

    }
}


class VriheddSappers : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>11 Strength</u>, Elf, Soldier, Bronze, Rare.
*Ambush: After 2 turns, flip over.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class VriheddVanguard : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = """Patch|May 22, 2018|0.9.24 Swap Update.
*6 Strength, Elf, Soldier, Bronze, Rare.
*Boost Elf allies by 1. Whenever you <u>Swap</u> this card, trigger its ability. <u>Note: Swap will no longer trigger during redraw phases</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


class Wardancer : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Skojatel, Tag.Elf, Tag.Soldier, Tag.Bronze)
    override val description = """Patch|May 22, 2018|0.9.24 Swap Update.
*3 Strength, Elf, Soldier, Bronze, Common.
*Whenever you <u>Swap</u> this unit, play it automatically on a random row. <u>Note: Swap will no longer trigger during redraw phases</u>.}}

"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}

