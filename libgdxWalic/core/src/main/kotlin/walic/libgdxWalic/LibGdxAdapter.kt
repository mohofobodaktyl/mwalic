package walic.libgdxWalic

import com.badlogic.gdx.scenes.scene2d.Stage
import kotlinx.coroutines.sync.Mutex
import walic.pureEngine.*
import kotlin.random.Random


class GetLater<A> {
    private var a: A? = null
    private val mutex = Mutex(true)


    fun set(a: A) {
        this.a = a
        mutex.unlock()
    }

    suspend fun get(f: suspend () -> Unit): A {
        mutex.lock()
        return a!!.apply { f() }
    }
}


class LibGdxAdapter(val stage: Stage, private val deck: List<String>?, val hostname: String, val initialRenderer: InitialRenderer) : PlayerAdapter() {    //graphics
    lateinit var state: State
    lateinit var sih: SecretInformationHandler
    var playerId: Int = -1

    lateinit var libGdxRenderer: LibGdxRenderer

    override fun setRequierd(state: State, noDeck: Boolean): Pair<List<String>, Long> {
        this.state = state
        this.sih = state.steamAdapter.secretInformationHandler
        this.playerId = state.steamAdapter.playerNumber
        libGdxRenderer = LibGdxRenderer(stage, state, playerId, sih, hostname, initialRenderer)
        val random = Random
        return Pair(
            when {
                noDeck -> listOf()
                deck == null -> {
                    val leader = allLeaders.random(random)
                    val fraction = leader.fraction!!
                    val possible = allNormalCards.filter { it.isFraction(fraction) }
                    val deck = mutableListOf(leader)
                    possible.filter { it.ist(Tag.Gold) }.pickUpToN(4, random).forEach { deck.add(it) }
                    possible.filter { it.ist(Tag.Silver) }.pickUpToN(6, random).forEach { deck.add(it) }
                    possible.filter { it.ist(Tag.Bronze) }.pickUpToN(5, random).forEach { card -> repeat(3) { deck.add(card) } }
                    deck.map { it::class.simpleName!! }
                }
                else -> deck
            }, random.nextLong())
    }

    override suspend fun pickRow(mask: RowMask): CardLocation {
        val ret = GetLater<CardLocation>()
        libGdxRenderer.appluFunction {
            lines.filter { mask.check(playerId, it.location) }.forEach { it.function = { ret.set(it.location);it.mod = LibGdxLineActor.Mods.Select } }
        }
        return ret.get {
            libGdxRenderer.appluFunction {
                lines.forEach { it.function = null }
            }
        }
    }


    override suspend fun pickPlaceBetweenCards(card: Card, mask: RowMask): CardLocation {
        val ret = GetLater<CardLocation>()
        libGdxRenderer.appluFunction {
            val actor = getActorOrCreate(card)
            mouseFollower = actor
            lines.filter { mask.check(playerId, it.location) }.forEach { it.mod = LibGdxLineActor.Mods.CatchCards }
            actor.function = {
                val p = actor.controller
                if (p is LibGdxLineActor) {
                    ret.set(p.location)
                    lines.forEach { it.mod = LibGdxLineActor.Mods.None }
                    mouseFollower = null
                    actor.function = null
                }
            }
        }
        return ret.get { }
    }

    class ButtonCard(string: String) : AbstractCard() {
        override val defaultStrenght = -1
        override val description = string
        override val tags = listOf(Tag.SPECIAL)
    }

    var lastGeneratedUUID = 50000L

    override suspend fun <A> pickFromList(list: List<A>, n: Int, canLess: Boolean): List<Int> {
        if (!canLess && list.size == n) {
            return list.indices.toList()
        }
        val ret = GetLater<List<Int>>()
        var cards = listOf<CardActor>()
        val valus = mutableListOf<Int>()

        libGdxRenderer.appluFunction {
            refrehs()
            cards = list.map {
                if (it is Card) {
                    otherCardsList.add(it)
                    getActorOrCreate(it)
                } else {
                    val card = Card(if (it is AbstractCard) it else ButtonCard(it.toString()), lastGeneratedUUID++, null)
                    otherCardsList.add(card)
                    CardActor(this, card)
                }
            }.mapIndexed { index, card ->
                card.function = {
                    if (valus.switch(index))
                        card.stateScale = 1f
                    else
                        card.stateScale = 0.7f
                    if (n != 1)
                        okButton.text = "${valus.size}${if (canLess) "</" else "/"}$n"
                    else
                        ret.set(valus)
                }
                card

            }
            okButton.function = {
                if (valus.size == n || (valus.size <= n && canLess))
                    ret.set(valus)
            }
            okButton.text = "${valus.size}${if (canLess) "</" else "/"}$n"
        }
        return ret.get {
            libGdxRenderer.appluFunction {
                otherCardsList.removeAll { true }
                cards.forEach { actor ->
                    actor.stateScale = 1f
                    actor.function = null
                }
                okButton.text = ""
                okButton.function = null
                openLibGdxStack?.switchState()
            }
        }
    }


    override suspend fun printMess(printOption: State.PrintOption, card: Card?) {
        libGdxRenderer.appluFunction {
            if (printOption is State.PrintOption.Draw) {
                printOption.drawn.forEach {
                    spawnCardOnSide(it, printOption.who)
                }
            }
            if (card != null) {
                if (printOption is State.PrintOption.Simple) {
                    if (printOption.mod == State.PrintOption.SingleMods.Spawned) {
                        val spawner = getActorOrCreate(card)
                        getActorOrCreate(printOption.other).setPosition(spawner.x, spawner.y)
                    }
                }
                spawnBulletFor(printOption, card)
            }
            if (printOption is State.PrintOption.AddToDeck) {
                if (printOption.playerId != playerId) {
                    enemyDeckCards.add(getActorOrCreate(printOption.card))
                }
            }
            if (printOption is State.PrintOption.Reveled) {
                if (printOption.final.ac != UnknowCard || !state.players[playerId].knownHand.contains(printOption.final)) {
                    tryToGetActor(printOption.initial)?.reveal(printOption.final)
                }
            }
            if (printOption is State.PrintOption.ApplyWether) {
                getActor(printOption.row).wetherText = if (printOption.weather != null)
                    printOption.weather!!::class.simpleName!!.camelToHumanRedable()
                else
                    ""
            }
            printMessage(printOption, card)
        }
    }
}