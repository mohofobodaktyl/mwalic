package walic.libgdxWalic

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions.*
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import com.soywiz.korio.net.AsyncClient
import com.soywiz.korio.net.AsyncServer
import com.soywiz.korio.stream.readS64LE
import com.soywiz.korio.stream.write64LE
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.withLock
import ktx.actors.onClick
import ktx.scene2d.scene2d
import ktx.scene2d.textButton
import walic.gwentBeta.defPort
import walic.libgdxWalic.Bullet
import walic.pureEngine.*
import kotlin.math.sin


const val defHeight = 100f
const val defWidth = 70f

const val xSep = 100f
const val ySep = 130f

const val pickButtonsX = 1000f

const val leaderX = defWidth / 2
lateinit var defSkin: Skin

var standardFont = BitmapFont()

var cardFront: TextureRegion = TextureRegion(Texture("Card.png"))
var cardBack: TextureRegion = TextureRegion(Texture("CardBack.png"))
val textEmotes = listOf(
    "Furthermore, I consider that Carthage must be destroyed",
    "Oh the times! Oh the customs",
    "It is an abomination",
    "You've come from the immortal realm of Barbelo",
    "So said Zarathustra",
    "Bioluminescent Central Intelligence Agency operative of afro-american descent",
    "Why, let the stricken deer go weep,\nThe hart ungalled play.\nFor some must watch, while some must sleep;\nSo runs the world away ",
    "***** ***",
    "The starry heavens above me and the moral law within me.",
    "A faithful German servant of Emperor Wilhelm I.",
    "Insert 60 page rant",
    "For nature cannot be fooled",
    "Exception that proves the rule",
    "This was a learned man and a lover of his country",
    "Appear weak when you are strong, and strong when you are weak.",
    "Artillery adds dignity, to what would otherwise be an ugly brawl",
    "You sockdologizing old man-trap!",
    "I envy the people who havent met you",
    "You’re impossible to underestimate.",
    "Entropy has unmade them",
    "The war situation has developed not necessarily to Japan's advantage",
    "Scientia imperii decus et tutamen",
    "I declare moral victory and quit",
    "All hail the BLÅHAJ",
    
)

class LibGdxRenderer(stage: Stage, val state: State, val playerId: Int, val sih: SecretInformationHandler, hostname: String, val initialRenderer: InitialRenderer) : Actor() {
    val outputQueue = Channel<Long>(Channel.CONFLATED)

    init {
        GlobalScope.launch {
            if (hostname != "auto") {
                val socket = when (hostname) {
                    "serwer" -> {
                        AsyncServer(defPort + 1, "0.0.0.0").accept()
                    }
                    else -> {
                        AsyncClient(hostname, defPort + 1)
                    }
                }
                println("Done aux")
                launch {
                    while (true) {
                        val p = outputQueue.receive()
                        socket.write64LE(p)
                    }
                }
                launch {
                    while (true) {
                        val s = socket.readS64LE()
                        globalMutex.withLock {
                            if (s < Long.MAX_VALUE / 2 || s == Long.MAX_VALUE)
                                currentHover = if (s == Long.MAX_VALUE) null else s
                            else {
                                val index = s - Long.MAX_VALUE / 2
                                val text = textEmotes[index.toInt()]
                                val textButton = TextButton(text, defSkin)
                                stage.addActor(textButton)

                                textButton.label.wrap = true
                                textButton.label.setAlignment(Align.center)
                                textButton.width = defWidth * 3.2f
                                textButton.height = defHeight * 1.5f
                                textButton.setPosition(midHidden - 4 * defWidth, 12 * defHeight)
                                textButton.addAction(
                                    sequence(
                                        moveTo(textButton.x, 8 * defHeight, 0.5f, Interpolation.pow2),
                                        delay(2f),
                                        moveTo(textButton.x, 12 * defHeight, 0.5f, Interpolation.pow2),
                                        removeActor()
                                    )
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    var currentHover: Long? = null

    fun getMouseCordinates(): Vector2 {
        return Vector2(Gdx.input.x.toFloat() / stage.root.scaleX, (stage.camera.viewportHeight - Gdx.input.y.toFloat()) / stage.root.scaleY)
    }

    init {
        stage.addActor(this)
    }

    var openLibGdxStack: LibGdxStackActor? = null
    suspend fun appluFunction(f: suspend LibGdxRenderer.() -> Unit) {
        globalMutex.withLock {
            f()
            refrehs()
        }
        return
    }

    var mouseFollower: CardActor? = null
    val mapCards = mutableMapOf<Long, CardActor>()

    fun getActorOrCreate(card: Card): CardActor {
        val index = state.players[playerId].knownHand.indexOf(card)
        if (index != -1) {
            return getActorOrCreate(sih.cardInHand[index])
        }
        val actor = (this.stage.actors.filterIsInstance<CardActor>() + lostCards).firstOrNull { it.card.uuid == card.uuid }
        if (actor != null) {
            actor.card = card
            return actor
        }
        return CardActor(this, card)
    }

    fun tryToGetActor(card: Card): CardActor? {
        val index = state.players[playerId].knownHand.indexOf(card)
        if (index != -1) {
            return tryToGetActor(sih.cardInHand[index])
        }
        val actor = (this.stage.actors.filterIsInstance<CardActor>() + lostCards).firstOrNull { it.card.uuid == card.uuid }
        if (actor != null) {
            actor.card = card
            return actor
        }
        return null
    }

    fun getActor(row: CardLocation): LibGdxLineActor {
        return stage.actors.filterIsInstance<LibGdxLineActor>().find { it.location.playerId == row.playerId && it.location.rowId == row.rowId }!!
    }

    fun spawnCardOnSide(card: Card, owner: Int) {
        getActorOrCreate(card).setPosition(midHidden, ySep * if (owner == playerId) 3 else 7)
    }


    var bigCardTextActor: Label
    var innerTable = Table(defSkin)
    var scrollPane = ScrollPane(innerTable)

    fun addText(text: String, replaceLast: Boolean) {
        if (!replaceLast) {
            val l = Label(text, defSkin)
            l.width = scrollPane.width
            l.wrap = true
            l.setAlignment(Align.center)

            innerTable.add(l).width(scrollPane.width)
            innerTable.row()
        } else {
            (innerTable.children.last() as Label).setText(text)
        }
        scrollPane.scrollTo(0f, 0f, 0f, 0f)
    }

    var wasLastWeek = false
    fun printMessage(printOption: State.PrintOption, card: Card?) {
        val text = printOption.toString() + if (card != null) " by $card" else ""
        println(text)
        if (printOption is State.PrintOption.Reveled && !printOption.shouldBePrinted) {
            return
        }
        if(card?.ist(Tag.SPAWNER)==true)
            return
        addText(text, wasLastWeek)
        wasLastWeek = printOption is State.PrintOption.InfoForPlayer
    }

    suspend fun spawnBulletFor(printOption: State.PrintOption, card: Card) {
        if (card.ist(Tag.SPAWNER))
            return
        val startActor = getActorOrCreate(card)
        when (printOption) {
            is State.PrintOption.WithTarget -> {
                spawnBullter(startActor, getActorOrCreate(printOption.other))
            }
            is State.PrintOption.Simpler -> {
                when (printOption.mod) {
                    State.PrintOption.SimplerMods.ActionOnDeck -> {
                        spawnBullter(startActor, graviards[card.owner])
                    }
                    State.PrintOption.SimplerMods.GetInfoAboutInitialDeck -> {
                        spawnBullter(startActor, graviards[card.owner])
                    }
                    State.PrintOption.SimplerMods.BoostRandomInHand -> {
                        spawnBullter(startActor, if (card.owner == playerId) yourHand else enemyHand)
                    }
                    State.PrintOption.SimplerMods.BoostAllInDeckAndHand -> {
                        spawnBullter(startActor, if (card.owner == playerId) yourHand else enemyHand)
                        spawnBullter(startActor, if (card.owner == playerId) yourDeck else enemyDeck)
                    }
                    State.PrintOption.SimplerMods.ApplyActionAndMoveToTop -> {
                    }
                    State.PrintOption.SimplerMods.InHandValue -> {
                        spawnBullter(startActor, if (card.owner == playerId) yourHand else enemyHand)
                    }
                    State.PrintOption.SimplerMods.CreateCardFromEnemyStartingDeck -> spawnBullter(startActor, graviards[1 - card.owner])
                    State.PrintOption.SimplerMods.GetInfoAboutHand -> spawnBullter(startActor, if (card.owner == playerId) yourHand else enemyHand)
                }
            }
            is State.PrintOption.ApplyWether -> {
                spawnBullter(startActor, getActor(printOption.row))
            }
            is State.PrintOption.Draw -> {
                printOption.drawn.forEach {
                    spawnBullter(startActor, getActorOrCreate(it))
                }
            }
            is State.PrintOption.Reveled -> {
                if (printOption.shouldBePrinted) {
                    val target = tryToGetActor(printOption.final) ?: tryToGetActor(printOption.initial)
                    if (target != null)
                        spawnBullter(startActor, target)
                }
            }
            is State.PrintOption.Transform -> {
                spawnBullter(startActor, getActorOrCreate(printOption.to))
            }
            is State.PrintOption.Dueal -> {
                spawnBullter(startActor, getActorOrCreate(printOption.first))
                spawnBullter(startActor, getActorOrCreate(printOption.secound))
            }
            is State.PrintOption.Drain -> {
                spawnBullter(startActor, getActorOrCreate(printOption.target))
                spawnBullter(getActorOrCreate(printOption.target), getActorOrCreate(printOption.reciver))
            }
            is State.PrintOption.Other -> {

            }
            is State.PrintOption.InfoForPlayer -> {

            }
            is State.PrintOption.AddToDeck -> {

            }
        }
    }

    suspend fun spawnBullter(start: Actor, end: Actor, reversDir: Boolean = false) {
        val bullets = stage.actors.filterIsInstance<Bullet>().filter { it.start != start || it.end == end }
        if (bullets.any()) {
            val dealyTime = bullets.map { (it.timeScale).toLong() }.maxOrNull()!!
            globalMutex.unlock()
            delay(1000 * dealyTime)
            globalMutex.lock()
        }
        Bullet(start, end, Color.RED, stage)
    }

    init {
        stage.addActor(scrollPane)
        scrollPane.width = 8 * defWidth
        scrollPane.height = 4 * ySep
        scrollPane.setPosition(pickButtonsX, 3f * ySep)


        bigCardTextActor = Label("", defSkin)
        bigCardTextActor.setPosition(pickButtonsX, 2 * ySep)
        bigCardTextActor.width = 8 * defWidth
        bigCardTextActor.setAlignment(Align.center)
        stage.addActor(bigCardTextActor)

    }

    val otherCardsList = mutableListOf<Card>()
    val enemyDeckCards = mutableListOf<CardActor>()


    private val graviards = state.players.mapIndexed { index, player -> LibGdxStackActor(this, { player.graveyard }, "Graveyard", ySep * (if (index == playerId) 1 else 6)) }
    private val leaders = state.players.mapIndexed { index, player -> LeaderActor(this, if (index == playerId) 0f else ySep * 7) { player.leader } }
    private val yourDeck = LibGdxStackActor(this, { sih.cardToDraw.sortedBy { -sih.cardAtTop.indexOf(it) + sih.cardAtBottom.indexOf(it) } }, "Your Deck", ySep * 3)
    private val banished = LibGdxStackActor(this, { state.players.map { it.banished }.flatten() }, "Banished", ySep * 4)

    private val otherCards = LibGdxStackActor(this, { otherCardsList }, "Other Cards", ySep * 2)
    private val enemyDeck = LibGdxStackActor(this, { enemyDeckCards.map { it.card } }, "Enemy Deck", 7 * ySep)

    val okButton = Button().apply {
        setPosition(midHidden, 0f)
        this@LibGdxRenderer.stage.addActor(this)
    }

    init {
        val messPanel = Table(defSkin)
        val messScrollPane = ScrollPane(messPanel, defSkin)
        messScrollPane.width = 4 * defWidth
        messScrollPane.height = 4 * defHeight
        textEmotes.forEachIndexed { index, string ->
            val l = TextButton(string, defSkin)
            l.label.wrap = true
            l.label.setAlignment(Align.center)
            l.onClick {
                outputQueue.offer(Long.MAX_VALUE / 2 + index)
            }
            messPanel.add(l).width(messScrollPane.width * 0.8f)
            messPanel.row()
        }
        stage.addActor(messScrollPane)
        messScrollPane.setPosition(midHidden - 5 * defWidth, -10 * defHeight)
        var isHidden = true
        scene2d.textButton("Messages", skin = defSkin) {
            onClick {
                isHidden = !isHidden
                messScrollPane.addAction(moveTo(messScrollPane.x, if (isHidden) -10 * defHeight else defHeight, 0.3f, Interpolation.pow2))
            }
            width = defWidth
            height = defHeight
            setPosition(midHidden - 1 * xSep, 0f)
            stage.addActor(this)
        }
        scene2d.textButton("Quit", skin = defSkin) {
            onClick {
                stage.clear()
                initialRenderer.create()
            }
            width = defWidth
            height = defHeight
            setPosition(midHidden - 2 * xSep, 0f)
            stage.addActor(this)
        }
    }

    private val playerInfo = listOf(TextActor(), TextActor())

    init {
        playerInfo.forEachIndexed { index, textActor ->
            stage.addActor(textActor)
            textActor.setPosition(leaderX + defWidth / 2, ySep * if (index == playerId) 1.5f else 6.5f)
        }
    }

    private val enemyHand: LibGdxLineActor
    val lines: List<LibGdxLineActor>
    private val yourHand: LibGdxLineActor

    init {
        var heightVal = -1
        val lists = mutableListOf<Pair<List<Card>, String>>(Pair(sih.cardInHand, "Your\nHand"))
        val names = listOf("Support", "Range", "Attack")


        lists.addAll(state.players[playerId].rows.mapIndexed { i, list -> Pair(list, "Your\n${names[i]}") })
        lists.addAll(state.players[1 - playerId].rows.reversed().mapIndexed { i, list -> Pair(list, "Enemy\n${names[2 - i]}") })
        lists.add((Pair(state.players[1 - playerId].knownHand, "Enemy\nHand")))

        val tempLine = lists.map { (p, s) ->
            LibGdxLineActor(this, p, s)
        }
        tempLine.forEach { it.setPosition(500f, ySep * (1 + heightVal++)) }

        lines = tempLine.drop(1).dropLast(1)
        lines.forEachIndexed { index, lineActor ->
            if (index < 3)
                lineActor.location = CardLocation(playerId, index)
            else
                lineActor.location = CardLocation(1 - playerId, 2 - index % 3)
        }
        yourHand = tempLine[0]
        enemyHand = tempLine.last()
    }

    val lostCards = mutableListOf<CardActor>()

    val handelers = this.stage.actors.filterIsInstance<LibGdxCardsHandeler>().sortedBy { listOf(enemyDeck, otherCards).indexOf(it) }

    fun refrehs() {
        this.stage.actors.filterIsInstance<CardActor>().forEach {
            it.reveled = false
            it.controller = null
        }
        handelers.forEach { it.refersh() }
        this.stage.actors.filterIsInstance<CardActor>().forEach {
            if (it.controller == null && it != mouseFollower) {
                it.remove()
                lostCards.add(it)
            }
        }
        handelers.filterIsInstance<LibGdxStackActor>().filter { stackActor -> stackActor.listCardActor.any { it.function != null } && stackActor.isHidden }.apply { if (size == 1) first().switchState() }

        state.players.forEachIndexed { index, player ->
            playerInfo[index].text = "Strength: ${player.getStrenght()}\n Wins:${player.wins}\n Passed: ${player.didPass}\n"
        }
        state.players[playerId].knownHand.forEach { getActorOrCreate(it).reveled = it.ac !is UnknowCard }
        sih.cardAtBottom.filter { sih.cardToDraw.contains(it) }.forEach { getActorOrCreate(it).reveled = true }
        sih.cardAtTop.filter { sih.cardToDraw.contains(it) }.forEach { getActorOrCreate(it).reveled = true }
        mouseFollower?.setPosWithAniamtion(0f, 0f, 1f)


    }

    var timeOscScale = 0f
    private var timeScale = 0f

    override fun draw(batch: Batch?, parentAlpha: Float) {
        val delta = Gdx.graphics.deltaTime
        timeScale += delta
        timeOscScale = (sin(timeScale * 5f) + 10) / 10f
        val mf = mouseFollower
        if (mf != null && mf.controller == null) {
            val pos = getMouseCordinates()
            mf.setPosWithAniamtion(pos.x - defWidth / 2, pos.y - defHeight / 2, 0f)
        }
    }
}


